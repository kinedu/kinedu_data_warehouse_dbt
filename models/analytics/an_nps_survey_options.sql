{{ config(materialized='table') }}

SELECT 
	n.survey_id, n.user_id, n.account_id, n.nps_created_at, n.nps, n.comment,
	n.is_born, n.baby_age, n.baby_id, n.gestational_weeks, n.pregnancy_id, n.os,
	n.app_version, n.kinedu_language, n.country, n.kinedu_region, n.signup_date,
	n.signup_provider, n.network, u.nps_option_id, n.product
FROM
    `celtic-music-240111.aws_kinedu_app_import.nps_survey_options` u
    JOIN {{ ref('an_nps_survey') }} n ON n.survey_id = u.nps_survey_id
ORDER BY n.nps_created_at