
with u as (
    select
        *
    from
        `celtic-music-240111`.`dbt_prod_base`.`ba_user_data`
),

ue as (
    select
        *
    from
        `celtic-music-240111`.`dbt_prod_caf`.`caf_user_experiments`
),

b as (
    select
        *
    from
        `celtic-music-240111`.`dbt_prod_caf`.`caf_babies_data`
),

/* m as (
    select
        *
    from
        `celtic-music-240111`.`dbt_prod_base`.`ba_mixpanel_data`
    where
        mp_time >= '2020-01-01'
),
*/
ts as (
    select
        *
    from
        `celtic-music-240111`.`dbt_prod_inter`.`in_track_subscriptions_preprocessing`
    where
        new_paying_user = 1
),

joined_datasources as (
    SELECT
        u.user_id,
        country,
        kinedu_region,
        kinedu_language,
        os,
        network,
        signup_date,
        cancellation_date,
        trial_start_date,
        trial_converted_date,
        premium_conversion_date,
        experiment_name,
        experiment_type,
        user_joined_experiment_date,
       # initial_assessment_completed,
        #event_name,
        #mp_time,
        #type_of_event,
        #date_diff(date(mp_time), signup_date, day) as event_time_since_signup,
        sale_date,
        sku,
        plan_type,
        usd_amount,
        date_diff(signup_date, b.baby_birthday, month) as baby_age_at_signup,
        baby_id,
        payment_source
    from
        u
        join ue using (user_id)
        left join b using (user_id)
     #   left join m using (email)
        left join ts on u.user_id = ts.user_id

),


final as (
    SELECT
        user_id,
        country,
        kinedu_region,
        kinedu_language,
        os,
        network,
        signup_date,
        baby_age_at_signup,
        baby_id,
        payment_source,
        max(cancellation_date) as cancellation_date,
        max(trial_start_date) as trial_start_date,
        max(trial_converted_date) as trial_converted_date,
        max(premium_conversion_date) as premium_conversion_date,
        experiment_name,
        experiment_type,
        min(user_joined_experiment_date) as user_joined_experiment_date,
       # max(case when type_of_event = 'activation' then true else false end) as activated,
        #max(case when type_of_event = 'activation' and event_time_since_signup <= 3 then true else false end) as three_day_retention,
        #max(case when type_of_event = 'activation' and event_time_since_signup <= 7 then true else false end) as one_week_retention,
        #max(case when type_of_event = 'activation' and event_time_since_signup > 7 and event_time_since_signup <= 14 then true else false end) as two_week_retention,
        #max(case when type_of_event = 'activation' and event_time_since_signup <= 30 then true else false end) as one_month_retention,
        #max(case when type_of_event = 'activation' and event_time_since_signup > 30 and event_time_since_signup <= 61 then true else false end) as two_month_retention,
        #max(plan_type) as plan_type,
        #max(sale_date) as sale_date,
        #max(sku) as sku,
        cast(max(usd_amount) as float64) as usd_amount
FROM 
        joined_datasources
WHERE 
        sale_date >= user_joined_experiment_date
GROUP BY
        user_id,
        country,
        kinedu_region,
        kinedu_language,
        os,
        network,
        signup_date,
        baby_age_at_signup,
        baby_id,
        payment_source,
        experiment_name,
        experiment_type

)



select * from final
