with final as (
    select
        user_id,
        os,
        country,
        signup_provider,
        kinedu_region,
        signup_date,
        premium_conversion_date,
        trial_charge,
        premium_conversion_charge,
        kinedu_language,
        play_trial_start_date,
        play_trial_converted_date,
        play_ft_payment_source,
        play_trial_sku,
        learn_trial_start_date,
        learn_trial_converted_date,
        learn_ft_payment_source,
        learn_trial_sku,
        auto_renew_status,
        case
            when play_trial_start_date is not null and learn_trial_start_date is not null then 'both trials'
            when play_trial_start_date is not null and learn_trial_start_date is null then 'play'
            when learn_trial_start_date is not null and play_trial_start_date is null then 'learn'
            when learn_trial_start_date is null and play_trial_start_date is null then 'no free trial'
        end as product
    from
       {{ ref('ba_user_data')}}
)

select * from final
