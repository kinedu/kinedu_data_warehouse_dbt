with u as (
    select
        user_id,
        country,
        os,
        kinedu_language,
        mp_version,
        mp_app,
        signup_date,
        network,
        premium_conversion_date,
        cancellation_date
    from 
        {{ref('ba_user_data')}} 
    order by user_id
),

first_sale as (
    select
        user_id,
        id,
        sale_date,
        usd_amount,
        payment_source

    from
         {{ref('in_first_sale_user')}} 
),

final as (
    select
        *
    from
        u left join first_sale
        using(user_id)
)

select * from final