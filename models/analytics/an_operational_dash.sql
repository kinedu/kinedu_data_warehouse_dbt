{{ config( materialized = "table") }}
with op as (
    select
        date,os,network,country, kinedu_language, product, plan_type,
        sum(signups) as signups,
        sum(fts_started) as fts_started,
        sum(fts_converted) as fts_converted,
        sum(impressions) as impressions,
        sum(spend) as spend,
        sum(clicks) as clicks,
        sum(total_spend_retargeting) as total_spend_retargeting,
        sum(signups_retargeting) as signups_retargeting,
        sum(new_subscriptions) as new_subscriptions,
        sum(new_subscriptions_sales) as new_subscriptions_sales,
        sum(renewals_yearly_ios) as renewals_yearly_ios,
        sum(renewals_sales_yearly_ios) as renewals_sales_yearly_ios,
        sum(renewals) as renewals,
        sum(renewals_sales) as renewals_sales,
        sum(downloads) as downloads,
        sum(other_sales) as other_sales,
        sum(other_purchases) as other_purchases,
        sum(refunds_total_amount_refund_date) as refunds_total_amount_refund_date,
        sum(num_of_refunds_refund_date) as num_of_refunds_refund_date,
        sum(refunds_total_amount_sale_date) as refunds_total_amount_sale_date,
        sum(num_of_refunds_sale_date) as num_of_refunds_sale_date,
        sum(refunds_total_amount_refund_date_ft_conv) as refunds_total_amount_refund_date_ft_conv,
        sum(num_of_refunds_refund_date_ft_conv) as num_of_refunds_refund_date_ft_conv,
        sum(refunds_total_amount_sale_date_ft_conv) as refunds_total_amount_sale_date_ft_conv,
        sum(num_of_refunds_sale_date_ft_conv) as num_of_refunds_sale_date_ft_conv
    from
        {{ ref('in_operational_by_date')}}
    group by
        date,os,network,country, kinedu_language, product, plan_type
),

kr as (
  select
    kinedu_region,
    country
  from
    `celtic-music-240111.dbt_prod_aux.countries_kinedu_regions`
),

final as (
    select
        *
    from
        op
        left join kr using (country)
)

select * from final