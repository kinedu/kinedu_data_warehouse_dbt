-- If this is being used in a dashboard, change it to a select of ba_user_data.
select
   user_id,
   email,
   country,
   os,
   mp_version,
   kinedu_language,
   mp_app,
   signup_date,
   network,
   premium_conversion_date,
   cancellation_date
from
    {{ref('ba_user_data')}}
order by user_id
