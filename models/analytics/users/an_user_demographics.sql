-- Only keep one baby per user
with b as (
    select 
      * 
    from 
      {{ ref('caf_babies_data')}}
    where baby_id in
      (
        select 
          max(baby_id) as baby_id
        from
          {{ ref('caf_babies_data')}}
        group by user_id
      )
),

-- Only keep one row per user_id (there is extra data duplicated for users)
u as (
    select 
      *
    from
      {{ ref('ba_user_data')}}
),

user_demographics as (
    select 
      u.user_id,
      u.email,
      u.country,
      u.os,
      u.signup_date,
      u.network,
      b.weeks_before_birth,
      b.baby_birthday,
      u.premium_conversion_date,
      u.trial_start_date
    from
      b
      join u using (user_id)
)

select * from user_demographics
 