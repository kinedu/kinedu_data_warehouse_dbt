with bs as (
  select
    *,
    case when sku like '%learn%' then 'learn'
    when sku like '%thrive%' then 'thrive'
    else 'play'
    end as product ## this is to add filter for product 
  from
    {{ ref('ba_sales')}}
),

iud as (
  select
    *
  from
    {{ ref('in_users_demographics')}}
),

final as (
    select 
        *
    from bs 
    left join iud using(user_id)
)

select * from final  