{{ config( materialized = "table") }}

WITH latest_sale AS
(
    select s.user_id, s.sale_date, s.sku, s.usd_amount, s.payment_source
from {{ ref('ba_sales')}} s
inner join (
    select user_id, max(sale_date) as MaxDate
    from {{ ref('ba_sales')}} sm
    group by user_id
) sm on s.user_id = sm.user_id and s.sale_date = sm.MaxDate
)


SELECT DISTINCT auv.user_id, auv.baby_id, u.email, u.os, ls.sku, u.kinedu_language, u.region, u.signup_date,
ae.name as experiment_name, av.name as variant_name,
ls.sale_date, ls.usd_amount, ls.payment_source, u.auto_renew_status,
u.learn_trial_sku, u.learn_ft_payment_source, u.learn_trial_start_date, u.learn_trial_converted_date,
CASE
    WHEN (u.learn_trial_converted_date IS NOT NULL) AND (u.learn_trial_start_date IS NOT NULL) THEN "Converted" 
    WHEN (u.learn_trial_converted_date IS NULL) AND date_diff(current_date(), u.learn_trial_start_date, day) < 7 AND plan_key = "learn" THEN "In Free Trial" 
    ELSE "Not Converted"
    END AS Learn_Converted
FROM `celtic-music-240111.ke_prod.kinedu_experience_api_uservariants` auv
LEFT JOIN `celtic-music-240111.ke_prod.kinedu_experience_api_variants` av on auv.variant_id = av.id
LEFT JOIN `celtic-music-240111.ke_prod.kinedu_experience_api_experiments` ae ON ae.id = av.experiment_id
LEFT JOIN {{ ref('ba_user_data')}} u on auv.user_id = u.user_id
LEFT JOIN latest_sale ls on u.user_id = ls.user_id
WHERE u.signup_date IS NOT NULL
