with its as (
  select 
        sale_date,
        user_id,
        sku, 
        plan_type,
        plan_status,
        os,
        country,
        region,
        city,
        network,
        product 
  from 
        {{ref('in_track_subscriptions')}} 
  where 
        new_subscription = 1
), 

bud as (
  select 
        distinct user_id,
        cancellation_date 
  from {{ref('ba_user_data')}}
), 

final as (
  select 
    *
  from 
    its 
    left join bud using(user_id)
  )
  
select * from final

