{{ config( materialized = "view") }}

select 
    campaign_name,
    date,
    os,
    network,
    sum(impressions) as impressions,
    sum(clicks) as clicks,
    sum(spend) as spend,
    sum(new_users) as new_users,
    sum(ft_started) as ft_started,
    sum(ft_converted) as ft_converted,
    sum(conversions) as conversions,
    cast(sum(sales) as float64) as sales
from
    {{ ref('in_awareness_prep')}}
group by
    campaign_name,
    date,
    os,
    network









