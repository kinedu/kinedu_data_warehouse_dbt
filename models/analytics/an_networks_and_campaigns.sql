{{ config(materialized='view') }}

with ud as (
    select 
        user_id,
        network,
        adjust_campaign,    
        os,
        country,
        signup_date,
        trial_start_date,
        trial_converted_date,
        premium_conversion_date 
    from 
         {{ ref('ba_user_data')}}
)

select * from ud 