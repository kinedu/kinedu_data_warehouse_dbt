with ww_mx as (
  select
    c.amount,
    ba.description,
    c.created as charge_date
  from
    `stripe_ww_mx_import.charge` c
    join `stripe_ww_mx_import.balance_transaction` ba on c.balance_transaction_id = ba.id
  where
    c.refunded is false 
    and c.status='succeeded'
    -- and (c.description is null or c.description not like '%Subscription%')
),

ww_mx_grouped as (
  select
    sum(amount/100) as other_sales,
    count(0) as other_purchases,
    description,
    cast(timestamp_seconds(charge_date) as date) as date,
    'stripe' as network
  from
    ww_mx
  group by
    description,
    cast(timestamp_seconds(charge_date) as date),
    network
),

br as (
  select
    c.amount,
    ba.description,
    c.created as charge_date
  from
    `stripe_brazil_import.charge` c
    join `stripe_brazil_import.balance_transaction` ba on c.balance_transaction_id = ba.id
  where
    c.refunded is false 
    and c.status='succeeded'
    -- and (c.description is null or c.description not like '%Subscription%')
),

br_grouped as (
  select
    sum(amount/100) as other_sales,
    count(0) as other_purchases,
    description,
    cast(timestamp_seconds(charge_date) as date) as date,
    'stripe' as network
  from
    br
  group by
    description,
    cast(timestamp_seconds(charge_date) as date),
    network
),

leads as (
  select
    c.amount,
    ba.description,
    c.created as charge_date
  from
    `stripe_leads_import.charge` c
    join `stripe_leads_import.balance_transaction` ba on c.balance_transaction_id = ba.id
  where
    c.refunded is false
    and c.status='succeeded'
),

leads_grouped as (
  select
    sum(amount/100) as other_sales,
    count(0) as other_purchases,
    description,
    cast(timestamp_seconds(charge_date) as date) as date,
    'stripe' as network
  from
    leads
  group by
    description,
    cast(timestamp_seconds(charge_date) as date),
    network
),

all_united as (
  select
    *
  from
    ww_mx_grouped
  
  union all 

  select
    *
  from
    br_grouped
  
  union all

  select
    *
  from
    leads_grouped
  
)

select
    date,
    description,
    network,
    sum(other_sales) as other_sales,
    sum(other_purchases) as other_purchases
from
    all_united
group by
    date,
    description,
    network