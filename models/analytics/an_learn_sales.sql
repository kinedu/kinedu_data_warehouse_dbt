{{ config( materialized = "table") }}


with s as (
  select
    user_id,
    sale_date,
    id,
    sale_id,
    payment_processor,
    payment_source,
    sku,
    usd_amount,
    renewed_automatically,
    month_key,
    plan_type,
    is_partner_sku,
    case
      when plan_type = 'yearly' then 12
      when plan_type = 'monthly' then 1
      when plan_type = 'semesterly' then 6
      when plan_type = 'lifetime' then 1
      when plan_type = 'quarterly' then 3
      else 1
    end as service_months
  from
    {{ ref('ba_sales') }}
  where
    sku like "%learn%"
),

u as (
  select
    user_id,
    os,
    country,
    region,
    city,
    kinedu_region,
    signup_date,
    network,
    campaign_name,
    user_gender,
    kinedu_language,
    premium_conversion_date
  from
    {{ ref('ba_user_data') }}
),

final as (
  select
    *
  from
    s
    join u using (user_id)
)


select * from final

