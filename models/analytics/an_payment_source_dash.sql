with c as (
  select 
    id as charge_id,
    sale_id,
    payment_source,
    test_type,
    status,
    charge_date,
    user_id,
    case 
      when sku like "%learn%" then 'learn'
      else 'play'
    end as product
  from {{ ref('caf_charges') }}
),

s as (
  select 
    sale_id,
    renewed_automatically,
    sale_date,
    sku,
    cast(usd_amount as float64) as usd_amount
  from  {{ ref('caf_sales') }}
),

fs as (
  select 
    sale_id as fs_sale_id
  from {{ ref('in_first_sale_user') }}
),

u as (
  select 
    user_id,
    country,
    kinedu_language,
    os,
    experience_type,
    mp_plan,
    play_trial_start_date,
    learn_trial_start_date,
    play_trial_converted_date,
    learn_trial_converted_date,
    email as user_email
  from {{ ref('ba_user_data') }}
), 

c_u as (
  select 
    * 
  from c
  left join u
  using(user_id)
),

s_fs as (
  select 
    * 
  from s
  left join fs
  on s.sale_id = fs.fs_sale_id
),

rps as (
  select 
    *,
    if((s_fs.renewed_automatically = false),1,0) as is_new_subs,
    if(((s_fs.renewed_automatically = false) or (c_u.status = 'free_trial')),1,0) as new_subs_or_ft,
    (case when s_fs.sale_id = fs_sale_id then 1 else 0 end) as first_sale
  from c_u 
  left join s_fs
  using(sale_id)
  order by test_type desc
)

select * from rps