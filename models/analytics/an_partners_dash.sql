{{ config( materialized = "table") }}


with u as (
	select
		u.user_id,
		u.os,
		u.signup_date,
		u.country,
		u.learn_trial_start_date,
		u.learn_trial_converted_date,
		u.play_trial_start_date,
		u.play_trial_converted_date
	from
		{{ ref('ba_user_data') }} u
),

s as (
	select
		s.sale_id,
		s.user_id,
		-- s.params,
		JSON_EXTRACT_SCALAR(s.params, "$['id']") as partners_subscription_used_id_1,
        JSON_EXTRACT_SCALAR(s.params, "$['data'].object.id") as partners_subscription_used_id_2,
		JSON_EXTRACT_SCALAR(s.params, "$['license_id']") as partners_license_used_id,
		s.usd_amount,
		s.payment_processor,
		s.sku,
        s.payment_source
	from
		{{ ref('ba_sales') }} s
	where
		s.sku like "%partner%" 
        or s.payment_source like"%partner%"
),

sub as (
    select *,
    case when partners_subscription_used_id_1 like '%sub_%' then partners_subscription_used_id_1 else partners_subscription_used_id_2
    end as partners_subscription_used_id
    from s
),

us as (
	select
		*,
		ifnull(partners_subscription_used_id, partners_license_used_id) as promo_used_id,
		case
			when partners_subscription_used_id is not null then 'subscription'
			when partners_license_used_id is not null then 'license'
		end as promo_type
	from
		u join sub using (user_id)
)

select * from us