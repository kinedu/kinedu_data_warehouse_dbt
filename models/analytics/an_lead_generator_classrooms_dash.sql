with cpu as (
    select 
        * 
    from
    {{ ref('caf_classrooms_prospect_users')}} 
), 

u as (
    select 
        user_id as referrer_id,
        user_name,
	    user_lastname,
	    email as user_email,
	    kinedu_language,
        country,
        os,
        signup_date
    from 
        {{ ref('ba_user_data')}} 
),

b as (
    select
        baby_id,
        user_id as referrer_id,
        baby_birthday
    from 
        {{ ref('caf_babies_data')}} 
), 

final as ( 
    select 
        *,
        date_diff(u.signup_date, b.baby_birthday, month) as baby_age_at_signup
    from 
        cpu 
    left join u using (referrer_id)
    left join b using(referrer_id)
)

select * from final