with p as (
  select 
    *
  from 
    {{ ref('caf_mkt_promo_codes')}}
),

u as (
  select 
    user_id,
    sale_date,
    sku,
    num, 
    new_subscription,
    os, 
    country,
    kinedu_region,
    kinedu_language,
    network,
    paid_usd_amount,
    recurring,
    payment_source
  from 
        {{ ref('in_track_subscriptions')}}
  where payment_source = 'PromoCode' and num = 1
),

t as (
    select 
        *
    from 
        {{ ref('caf_user_promo_code_tries')}}
),

pre as (
  select
  *
  from p
  left join t using(mkt_promo_code_id)
  where flag = 1
),

final as (
  select 
  * 
  from u
  left join pre using(user_id) 
) 

select * from final