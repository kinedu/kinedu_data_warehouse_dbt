# materialize as a table
{{ config( materialized = "table") }}

/*each separation is to attribute the date according to the type, the spend is attributed to the date of sale, the new users to the date of creation, the trials to the date in which the free trial started */

with amount as (
select sale_date,campaign_name,adset_name,count(usd_amount) as purchases,sum(usd_amount) as usd_amount
from
(
SELECT date(u.created_at) as created_at, u.user_id, u.campaign_name, u.adset_name,t.sale_date, t.usd_amount
FROM {{ ref('ba_user_data') }} as u
left  join {{ ref('in_track_subscriptions') }}  as t
on u.user_id= t.user_id
where date(u.created_at)>'2020-01-01' and u.network	= 'Facebook' and t.new_subscription=1 
)
group by sale_date,campaign_name,adset_name 
),
/*purchases amount, attribution by sale date*/
users as(
SELECT date(created_at) as created_at ,campaign_name,adset_name,count( distinct user_id) as new_users,
FROM {{ ref('ba_user_data') }}
where date(created_at)>'2020-01-01' and campaign_name is not null and network	= 'Facebook'
group by created_at,campaign_name,adset_name 
), 
/*users, attribution by created_at*/

ft as ( SELECT trial_start_date,campaign_name, adset_name,count(trial_start_date) as trials
FROM {{ ref('ba_user_data') }}
where network	= 'Facebook'  and trial_start_date>'2020-01-01' 
group by trial_start_date,campaign_name,adset_name
),

/*ft, attribution by trial_start*/

spend as (
select
  date(date) as date,
  campaign_name,
  adset_name,
  network,
  sum(impressions) as impressions,
  sum(spend) as spend,
  sum(clicks) as clicks,
from
  {{ ref('caf_facebook_ads') }}
where date>= '2020-01-01' 
#business_unit='Kinedu'
group by  date,adset_name,campaign_name,network
),
/*spend, attribution by date*/

/*the main problem is that several adset names appear with more characters than normal */
adnames as(
SELECT campaign_name,adset_id, adset_name
FROM {{ ref('caf_facebook_ads') }}
where date>= '2020-01-01' 
group by campaign_name,adset_id,adset_name
),

/*We can have 2 cases:
- same name of adset- > different adset id's
- Different adset names -> same adset id's */
adset as(
with groupedds as (SELECT campaign_name,adset_id,max(date) as max_date
    FROM {{ ref('caf_facebook_ads') }}
where date>= '2020-01-01' 
    GROUP BY campaign_name,adset_id
),
ds as (
SELECT campaign_name,adset_id,max(date) as date, adset_name
FROM {{ ref('caf_facebook_ads') }}
where date>= '2020-01-01' 
group by campaign_name,adset_id,adset_name 
),
spe as (
SELECT campaign_name,adset_id,count(spend) as spendcount
FROM {{ ref('caf_facebook_ads') }} 
where date>= '2020-01-01' 
group by date,campaign_name,adset_id
)
select groupedds.campaign_name,groupedds.adset_id, ds.adset_name, cast(avg(spe.spendcount) as int64) as spend_count
from
groupedds
left join ds 
on groupedds.campaign_name=ds.campaign_name and groupedds.adset_id=ds.adset_id and groupedds.max_date=ds.date
left join spe 
on groupedds.campaign_name=spe.campaign_name and groupedds.adset_id=spe.adset_id 
group by campaign_name,adset_id,ds.adset_name
),
/*group by AdsetId and assign as name the most recent adset name */

days as (
SELECT campaign_name,min(date) as min_camp,max(date) as max_camp
FROM {{ ref('caf_facebook_ads') }} 
where date>= '2020-01-01' 
group by campaign_name
),

f as(
select
case
when spend.date is not null then spend.date
when amount.sale_date is not null then amount.sale_date
when users.created_at  is not null then users.created_at
when ft.trial_start_date is not null then ft.trial_start_date
end
as date,
#spend.date as camp_date,
case
when spend.campaign_name is not null then spend.campaign_name
when amount.campaign_name is not null then amount.campaign_name
when users.campaign_name  is not null then users.campaign_name
when ft.campaign_name is not null then ft.campaign_name
end
as campaign_name,

case
when spend.adset_name is not null then spend.adset_name
when amount.adset_name is not null then amount.adset_name
when users.adset_name  is not null then users.adset_name
when ft.adset_name is not null then ft.adset_name
end
as adset_name,
#spend.network,
spend.impressions,spend.clicks,spend.spend, 
amount.usd_amount,amount.purchases,
users.new_users,ft.trials	
from spend
full outer join amount 
on  spend.campaign_name=amount.campaign_name and spend.adset_name=amount.adset_name and spend.date= amount.sale_date
full outer join users
on spend.campaign_name=users.campaign_name and spend.adset_name=users.adset_name  and spend.date= users.created_at
full outer join ft 
on spend.campaign_name=ft.campaign_name and spend.adset_name=ft.adset_name and spend.date= ft.trial_start_date
)

select
date,
f.campaign_name,
adset.adset_id,
adset.adset_name,
days.min_camp,
days.max_camp,
date_diff(date, days.min_camp, day) as days,
  case
    when f.campaign_name like "%iOS%" 
    or f.campaign_name like "%IOS%" 
    or f.campaign_name like "%iOs%" then 'iOS'
    when 
      f.campaign_name like "%Android%" 
      or f.campaign_name like "%ANDROID%"
      or f.campaign_name like '%aOS%'
      or f.campaign_name like "%android%" then 'Android'
      end as os,
  Case 
  when f.campaign_name like "%(i-p)%" then 'Purchase'
  when f.campaign_name like "%(i-r)%" then 'Complete Registration'
  when f.campaign_name like "%(i-ft)%" then 'Free Trial'
  when f.campaign_name like "%(i)%" then 'Installs'
  end
  as Target,
  Case 
    when f.campaign_name like "%(i-ft)%" then 'Lower Funnel'
  when f.campaign_name like "%(i-p)%" then 'Lower Funnel'
  when f.campaign_name like "%(i)%" then 'Upper Funnel'
  when f.campaign_name like "%(i-r)%" then 'Upper Funnel'
  end
  as Focus,
new_users,
spend,
adset.spend_count,
trials,
impressions,
clicks,
purchases,
usd_amount,
from f
left JOIN  adnames
on   f.campaign_name= adnames.campaign_name and  f.adset_name LIKE CONCAT(adnames.adset_name,'%') 
/*There is a problem, the adset names in some cases not only have extra characters, they also have a different part e.g. Female vs Fm. I tried a substring(f.adset_name,0,22) but it didn't work, it gave me more nulls. */
left JOIN  adset
on   f.campaign_name= adset.campaign_name and adnames.adset_id= adset.adset_id
left join days 
on f.campaign_name=days.campaign_name
where (f.campaign_name not like "%Test%" and f.campaign_name  not like "%Retargeting%") AND (f.campaign_name like "%(i-ft)%"  or f.campaign_name like "%(i-p)%" or f.campaign_name like "%(i)%" or f.campaign_name like "%(i-r)%")  AND (adset.adset_name IS NOT NULL) 
order by date,campaign_name,adset_id
/*since the f.adsetname is different from the real one (it has more characters) 
we make a join to have the short name and to be able to make the join to get the adset id
It is not direct because adnames have all the adset names while adset has the ones grouped with the most recent adname*/