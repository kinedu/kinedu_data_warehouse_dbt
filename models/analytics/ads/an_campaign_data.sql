# materialize as a table
{{ config( materialized = "table") }}



with amount as ( select sale_date,campaign_name,count(usd_amount) as purchases,sum(usd_amount) as usd_amount
FROM 
{{ ref('in_track_subscriptions') }} 
where campaign_name is not null and sale_date>='2020-01-01' and new_subscription = 1 and campaign_name like "%FB%"
group by sale_date,campaign_name
order by campaign_name
), 
users as (
SELECT date(created_at) as created_at, campaign_name,count(distinct user_id) as new_users
FROM 
{{ ref('ba_user_data') }}
where date(created_at)>'2020-01-01' and campaign_name is not null and campaign_name like "%FB%"
group by date(created_at), campaign_name

),
ft as ( SELECT trial_start_date, campaign_name,count(trial_start_date) as trials
FROM {{ ref('ba_user_data') }}
where trial_start_date>'2020-01-01' 

group by trial_start_date, campaign_name
),
spend as (
select
  date(date) as date,
  campaign_name,
  network,
  sum(impressions) as impressions,
  sum(spend) as spend,
  sum(clicks) as clicks,
from
  {{ ref('caf_facebook_ads') }}
where campaign_name in ('20.09.14 - FB - Android (i-p)','20.09.14 - FB - iOS (i-ft)','20.09.21 - FB - iOS - CC (i)','20.09.21 - FB - Android - CC (i)','20.09.29 - FB - Android (i-ft)') and date>= '2020-01-01' 
group by  date,campaign_name,network
order by date, campaign_name
),
f as(
select
case
when spend.date is not null then spend.date
when amount.sale_date is not null then amount.sale_date
when users.created_at  is not null then users.created_at
when ft.trial_start_date is not null then ft.trial_start_date
end
as date,
spend.date as camp_date,
case
when spend.campaign_name is not null then spend.campaign_name
when amount.campaign_name is not null then amount.campaign_name
when users.campaign_name  is not null then users.campaign_name
when ft.campaign_name is not null then ft.campaign_name
end
as campaign_name,
spend.network,
spend.impressions,spend.clicks,spend.spend,amount.usd_amount,
amount.purchases ,users.new_users,ft.trials	
from spend
full outer join amount 
on  spend.campaign_name=amount.campaign_name and spend.date= amount.sale_date
full outer join users
on spend.campaign_name=users.campaign_name and spend.date= users.created_at
full outer join ft 
on spend.campaign_name=ft.campaign_name and spend.date= ft.trial_start_date
order by date, campaign_name
)

select
date,
campaign_name,
  case
    when campaign_name like "%iOS%" 
    or campaign_name like "%IOS%" 
    or campaign_name like "%iOs%" then 'iOS'
    when 
      campaign_name like "%Android%" 
      or campaign_name like "%ANDROID%"
      or campaign_name like '%aOS%'
      or campaign_name like "%android%" then 'Android'
      end as os,
  Case 
  when campaign_name like "%(i-p)%" then 'Purchase'
  when campaign_name like "%(i-r)%" then 'Complete Registration'
  when campaign_name like "%(i-ft)%" then 'Free Trial'
  when campaign_name like "%(i)%" then 'Installs'
  end
  as Target,
  Case 
    when campaign_name like "%(i-ft)%" then 'Lower Funnel'
  when campaign_name like "%(i-p)%" then 'Lower Funnel'
  when campaign_name like "%(i)%" then 'Purchases'
  when campaign_name like "%(i-r)%" then 'Purchases'
  end
  as Focus,
new_users,
spend,
trials,
impressions,
clicks,
purchases,
usd_amount,
from f
where campaign_name in ('20.09.14 - FB - Android (i-p)','20.09.14 - FB - iOS (i-ft)','20.09.21 - FB - iOS - CC (i)','20.09.21 - FB - Android - CC (i)','20.09.29 - FB - Android (i-ft)')
order by date desc