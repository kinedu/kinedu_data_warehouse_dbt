with a as (
    select
        date,
        country,
        os,
        network,
        campaign_name,
        sum(spend) as spend
    from
      {{ ref('ba_ad_spends') }}
    group by
        date,
        country,
        os,
        network,
        campaign_name 
),
users as (
    select
        u.signup_date as date,
        u.country,
        c.os,
        c.network,
        u.campaign_name,
        count(distinct user_id) as num_users,
        date_diff(u.trial_start_date, u.signup_date, day) as difdays
    from
        {{ ref('ba_user_data')}} u left join {{ ref('in_campaigns')}} c
        using(campaign_name)
    where
        u.trial_start_date is not null
    group by
        u.signup_date,
        u.country,
        c.os,
        c.network,
        u.campaign_name,
        difdays
),

r as (
    select
        *
    from
        `celtic-music-240111.dbt_prod_aux.countries_kinedu_regions`
),

preprocessing as (
    select
        *
    from a full join users
    using(date,country,os,network,campaign_name)
),

final as (
    select
        preprocessing.*,
        r.kinedu_region
    from
        preprocessing left join r using (country)
)

select * from final