{{ config( materialized = "table") }}
with a as (
    select
        date,
        country,
        os,
        network,
        campaign_name,
        sum(spend) as spend
    from
      {{ ref('ba_ad_spends') }}
    group by
        date,
        country,
        os,
        network,
        campaign_name 
),
users as (
    select
        u.signup_date as date,
        u.country,
        u.os,
        c.network,
        u.campaign_name,
        -- this is using distinct sale_id instead of distinct user_id because the same user can have 2 new_subscriptions (play and learn)
        count(distinct sale_id) as num_users,
        cast(sum(usd_amount) as float64) as revenue,
        date_diff(u.sale_date, u.signup_date, day) as difdays
    from
        {{ ref('in_first_sale_user')}} u left join {{ ref('in_campaigns')}} c
        using(campaign_name)
    group by
        u.signup_date,
        u.country,
        u.os,
        c.network,
        u.campaign_name,
        difdays
),

regions as (
    select
        *
    from
        `celtic-music-240111.dbt_prod_aux.countries_kinedu_regions`
),

pre_final as (
    select
        *
    from a full join users
    using(date,country,os,network,campaign_name)
),

final as (
    select
        *
    from pre_final left join regions
    using (country)
)

select * from final