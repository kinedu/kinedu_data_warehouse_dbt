with ad as (
  select
    business_unit,
    network,
    campaign_name,
    cast(max(date) as date) as campaign_last_date,
    sum(clicks) as clicks,
    sum(impressions) as impressions,
    sum(spend) as spend
  from
    {{ ref('ba_ad_spends')}}
  group by
    business_unit,
    network,
    campaign_name
),

u as (
  select
    network,
    campaign_name,
    count(distinct user_id) as new_users,
    count(distinct if(trial_start_date is not null, user_id, null)) as trial_starts,
    count(distinct if(trial_converted_date is not null, user_id, null)) as trial_convertions,
    count(distinct if(premium_conversion_date is not null, user_id, null)) as conversions
  from
    {{ ref('ba_user_data')}}
  group by
    network,
    campaign_name
),

final as (
  select
    *
  from
    ad
    left join u using (network, campaign_name)
)

select * from final
