{{ config(materialized='table') }}

SELECT
    n.id as survey_id, u.user_id, u.account_id, n.created_at as nps_created_at, n.nps, n.comment,
    n.baby_age, n.baby_id, n.gestational_weeks, n.pregnancy_id, n.os, n.app_version, u.kinedu_language,
    u.country, u.kinedu_region, u.signup_date, u.signup_provider, u.network, 
    u.created_at as user_created_at,
    case 
        when plan_key like "%premium%" AND u.plan_expires_on > current_date() then "play"
        when plan_key like "%play%" AND u.plan_expires_on > current_date() then "play"
        when plan_key like "%learn%" AND u.plan_expires_on > current_date() then "learn"
        when plan_key like "free" then "freemium"
        else "freemium"
    end as product,
    case
        when n.baby_id is not null AND (n.pregnancy_id is null OR n.pregnancy_id = 0) then true
        when n.baby_id is null AND (n.pregnancy_id is not null OR n.pregnancy_id != 0) then false
        else null
    end as is_born
FROM
    `celtic-music-240111.aws_kinedu_app_import.nps_surveys` n
    JOIN {{ ref('ba_user_data') }} u on n.user_id = u.user_id
WHERE n.__hevo__marked_deleted = false 

/* AND (
    (n.os LIKE 'Android' AND n.app_version < '1.83.0' AND n.nps IS NOT NULL AND n.comment IS NOT NULL) OR
    (n.os LIKE 'Android' AND n.app_version >= '1.83.0' AND n.nps IS NOT NULL) OR
    (n.os LIKE 'iOS' AND nps IS NOT NULL AND n.comment IS NOT NULL)
    )*/