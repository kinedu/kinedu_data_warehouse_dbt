-- Signups and FT
select
    date,os,network,country, kinedu_language, product, plan_type,
    signups,
    fts_started,
    fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ ref('in_signup_ft_conversion_date_counts')}}

union all

-- Ad Spends
select
    date,os,network,country, kinedu_language, null as product, null as plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    impressions,
    spend,
    clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ ref('in_ad_spends_network_os_date')}}

union all

-- Ad Spends Retarget
select
    date,os,network,country, kinedu_language, null as product, null as plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    total_spend_retargeting,
    signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ ref('in_retarget_date') }}

union all

-- New subscriptions and renewals
select
    date,os,network,country, kinedu_language, product, plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    new_subscriptions,
    new_subscriptions_sales,
    renewals_yearly_ios,
    renewals_sales_yearly_ios,
    renewals,
    renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ ref('in_new_renewals_date') }}


union all 

select
    date,os, null as network,country, null as kinedu_language, null as product, null as plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ ref('in_downloads_date')}}

union all

--other sales from stripe (masterclass) and shopify (toys)
select 
    date, null as os, network, null as country, null as kinedu_language, null as product, null as plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    other_sales,
    other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from 
    {{ ref('in_other_sales')}}

union all

-- all sales (including FT converted)
-- refunds - refund date
select
    date,os,network,country,kinedu_language,product,plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    refunds_total_amount as refunds_total_amount_refund_date,
    num_of_refunds as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ref('in_refunds_date')}}
where
    date_type = "refund_date"

union all

-- refunds - sale date
select
    date,os,network,country,kinedu_language,product,plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    refunds_total_amount as refunds_total_amount_sale_date,
    num_of_refunds as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ref('in_refunds_date')}}
where
    date_type = "sale_date"


union all


-- FT converted
-- refunds - refund date
select
    date,os,network,country,kinedu_language,product,plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    refunds_total_amount as refunds_total_amount_refund_date_ft_conv,
    num_of_refunds as num_of_refunds_refund_date_ft_conv,
    0 as refunds_total_amount_sale_date_ft_conv,
    0 as num_of_refunds_sale_date_ft_conv
from
    {{ref('in_refunds_date')}}
where
    date_type = "refund_date"
    and refund_was_ft_converted = true

union all

-- refunds - sale date
select
    date,os,network,country,kinedu_language,product,plan_type,
    0 as signups,
    0 as fts_started,
    0 as fts_converted,
    0 as impressions,
    0 as spend,
    0 as clicks,
    0 as total_spend_retargeting,
    0 as signups_retargeting,
    0 as new_subscriptions,
    0 as new_subscriptions_sales,
    0 as renewals_yearly_ios,
    0 as renewals_sales_yearly_ios,
    0 as renewals,
    0 as renewals_sales,
    0 as downloads,
    0 as other_sales,
    0 as other_purchases,
    0 as refunds_total_amount_refund_date,
    0 as num_of_refunds_refund_date,
    0 as refunds_total_amount_sale_date,
    0 as num_of_refunds_sale_date,
    0 as refunds_total_amount_refund_date_ft_conv,
    0 as num_of_refunds_refund_date_ft_conv,
    refunds_total_amount as refunds_total_amount_sale_date_ft_conv,
    num_of_refunds as num_of_refunds_sale_date_ft_conv
from
    {{ref('in_refunds_date')}}
where
    date_type = "sale_date"
    and refund_was_ft_converted = true