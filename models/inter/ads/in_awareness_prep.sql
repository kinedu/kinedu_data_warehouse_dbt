select 
    campaign_name,
    date,
    os,
    network,
    sum(impressions) as impressions,
    sum(clicks) as clicks,
    sum(spend) as spend,
    null as new_users,
    null as ft_started,
    null as ft_converted,
    null as conversions,
    null as sales
from 
    {{ ref('ba_ad_spends')}}
group by 
    campaign_name,
    date,
    os,
    network

union all

select
    u.campaign_name,
    u.signup_date as date,
    c.os,
    c.network,
    null as impressions, 
    null as clicks, 
    null as spend,
    count(0) AS new_users,
    sum(case when u.trial_start_date is null then 0 else 1 end) AS ft_started,
    sum(case when u.trial_converted_date is null then 0 else 1 end) AS ft_converted,
    null as conversions,
    null as sales
from
    {{ ref('ba_user_data')}} u join  {{ ref('in_campaigns')}} c
    using(campaign_name)
group by 
    u.campaign_name,
    u.signup_date,
    c.os,
    c.network

union all

select
    u.campaign_name,
    u.signup_date as date,
    c.os,
    c.network,
    null as impressions, 
    null as clicks, 
    null as spend,
    null as new_users,
    null as ft_started,
    null as ft_converted,
    count(0) as conversions,
    sum(u.usd_amount) as sales
from
    {{ ref('in_first_sale_user')}} u join {{ ref('in_campaigns')}} c
    using(campaign_name)
group by
    u.campaign_name,
    u.signup_date,
    c.os,
    c.network
