select
  date,
  os,
  network,
  country,
  kinedu_language,
  sum(impressions) as impressions,
  sum(spend) as spend,
  sum(clicks) as clicks
from
  {{ ref('ba_ad_spends') }}
group by
  date,
  os,
  network,
  country,
  kinedu_language
