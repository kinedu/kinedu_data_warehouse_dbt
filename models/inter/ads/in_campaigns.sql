select 
    campaign_name,
    os,
    network
from 
    {{ ref('ba_ad_spends')}}
group by
    campaign_name,
    os,
    network