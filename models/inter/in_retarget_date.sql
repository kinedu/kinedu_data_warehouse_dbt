with aux as(
-- Ad Spends Retarget
select
    date,
    os,
    network,
    country,
    null as kinedu_language,
    null as signups_retargeting,
    sum(spend) as total_spend_retargeting
from
    {{ ref('ba_ad_spends') }}
where
    campaign_name like '%Retargeting%'
group by
    date,
    os,
    network,
    country

union all 

--Users Retarget
select
    signup_date as date,
    os,
    network,
    country,
    kinedu_language,
    count(0) as signups_retargeting,
    null as total_spend_retargeting
from
    {{ ref('ba_user_data')}}
where 
    adjust_campaign like '%Retargeting%' 
    or (adjust_campaign in ('19.11.26 - FB - iOS - US - Thanksgiving', '19.11.26 - FB - iOS - EN - Thanksgiving', '19.11.26 - FB - iOS - ES - Thanksgiving', '19.11.26 - FB - Android - US - Thanksgiving', '19.11.26 - FB - Android - EN - Thanksgiving', '19.11.26 - FB - Android - ES - Thanksgiving') and adset_name = "Current FR Users")
group by
    signup_date,
    os,
    network,
    country,
    kinedu_language
)

select 
    date,
    os,
    network,
    country,
    kinedu_language,
    sum(signups_retargeting) as signups_retargeting,
    sum(total_spend_retargeting) as total_spend_retargeting
from
    aux
group by
    date,
    os,
    network,
    country,
    kinedu_language

