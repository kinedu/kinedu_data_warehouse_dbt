  select
    signup_date as date,
    os,
    network,
    country,
    kinedu_language,
    cast(null as string) as product,
    cast(null as string) as plan_type,
    count(0) as signups,
    null as fts_started,
    null as fts_converted
  from
    {{ ref('ba_user_data')}}
  group by
    signup_date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    play_trial_start_date as date,
    os,
    network,
    country,
    kinedu_language,
    'play' as product,
    case
      when play_trial_sku like "%lifetime%" then 'lifetime'
      when play_trial_sku like "%_12%" then 'yearly'
      when play_trial_sku like "%_1%" then 'monthly'
      when play_trial_sku like "%_6%" then 'semesterly'
      when play_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    count(0) as fts_started,
    null as fts_converted
  from
    {{ ref('ba_user_data')}}
  group by
    play_trial_start_date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    learn_trial_start_date as date,
    os,
    network,
    country,
    kinedu_language,
    'learn' as product,
    case
      when learn_trial_sku like "%lifetime%" then 'lifetime'
      when learn_trial_sku like "%_12%" then 'yearly'
      when learn_trial_sku like "%_1%" then 'monthly'
      when learn_trial_sku like "%_6%" then 'semesterly'
      when learn_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    count(0) as fts_started,
    null as fts_converted
  from
    {{ ref('ba_user_data')}}
  group by
    learn_trial_start_date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    play_trial_converted_date as date,
    os,
    network,
    country,
    kinedu_language,
    'play' as product,
    case
      when play_trial_sku like "%lifetime%" then 'lifetime'
      when play_trial_sku like "%_12%" then 'yearly'
      when play_trial_sku like "%_1%" then 'monthly'
      when play_trial_sku like "%_6%" then 'semesterly'
      when play_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    null as fts_started,
    count(0) as fts_converted
  from
    {{ ref('ba_user_data')}}
  group by
    play_trial_converted_date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    learn_trial_converted_date as date,
    os,
    network,
    country,
    kinedu_language,
    'learn' as product,
    case
      when learn_trial_sku like "%lifetime%" then 'lifetime'
      when learn_trial_sku like "%_12%" then 'yearly'
      when learn_trial_sku like "%_1%" then 'monthly'
      when learn_trial_sku like "%_6%" then 'semesterly'
      when learn_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    null as fts_started,
    count(0) as fts_converted
  from
    {{ ref('ba_user_data')}}
  group by
    learn_trial_converted_date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type
