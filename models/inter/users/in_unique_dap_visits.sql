{{ config( materialized = "view") }}

with mp_union as (
select
    cast((DATETIME(TIMESTAMP_MILLIS(m.mp_processing_time_ms), "US/Pacific")) as date) as date,
    cast(null as string) as payment_source,
    u.os,
    u.network,
    u.country,
    u.kinedu_language,
    "dap" as mp_table
    --count(0) as unique_dap_visits
  from
    `celtic-music-240111.mixpanel_import.s_daphome` m
    left join  {{ ref('ba_user_data')}} u on cast(m._user_id as int)=u.user_id
    where u.plan_key not like '%learn%'

UNION ALL 

select
    cast((DATETIME(TIMESTAMP_MILLIS(t.mp_processing_time_ms), "US/Pacific")) as date) as date,
    cast(null as string) as payment_source,
    u.os,
    u.network,
    u.country,
    u.kinedu_language,
    "tap" as mp_table
  from
    `celtic-music-240111.mixpanel_import.s_taphome` t
    left join  {{ ref('ba_user_data')}} u on cast(t._user_id as int)=u.user_id
    where u.plan_key not like '%learn%'
    )

SELECT 
    date,
    cast(null as string) as payment_source,
    mp.os,
    mp.network,
    mp.country,
    mp.kinedu_language,
    cast(null as string) as product,
    cast(null as string) as plan_type,
    null as signups,
    null as fts_started,
    count(0) as unique_dap_visits
FROM mp_union mp
group by
    date,
    payment_source,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type
ORDER BY date