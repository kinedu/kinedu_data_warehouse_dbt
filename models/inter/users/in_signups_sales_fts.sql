-- The idea is to "stack" all of the dates where a user has signed up, had a FT and/or had a purchase
-- afterward a group by will be done to obtain the data from the user table and get a count by the different groups

-- user id will be the key that joins the different tables
-- calc_type is a variable that will help us identify what the count is for
with unioned_table as (
  --Fist obtain all of the New Subscription sales and identify what product it is and plan type
  select
    user_id, 
    case when sku like '%ft%' then DATE_SUB(sale_date, INTERVAL 1 WEEK)
    else sale_date
    end as date,
    payment_source,
    product,
    plan_type,
    "sales" as calc_type, 
    count(0) as total,
    'New Subscription' as payment_type
from
    {{ ref('in_first_sale_user')}}
  group by
    date,
    user_id,
    payment_source,
    product,
    plan_type,
    payment_type

UNION ALL 
--Obtain all of the Renewals sales that a user has had and also identify what product it is and plan type
 select
    user_id, 
    sale_date as date,
    payment_source,
    product,
    plan_type,
    "sales" as calc_type, 
    count(0) as total,
    'Renewal' as payment_type
from
    {{ ref('in_track_subscriptions')}}
where 
    new_subscription = 0 and num = 1
  group by
    date,
    user_id,
    payment_source,
    product,
    plan_type,
    payment_type

UNION ALL
-- Obtain all of the signups that kinedu has had by date
  select
    u.user_id, 
    signup_date as date,
    cast(null as string) as payment_source,
    cast(null as string) as product,
    cast(null as string) as plan_type,
    "signups" as calc_type, 
    count(0) as total,
    cast(null as string) as payment_type
  from
    `celtic-music-240111.dbt_prod_base.ba_user_data` u
  group by
    signup_date,
    u.user_id

UNION ALL
   --Obtain all of the FTs that a user has had and also identify what product it is and plan type.
   -- Need to do play first since the payment source and start dates are in different columns 
    select
    u.user_id, 
    play_trial_start_date as date,
    play_ft_payment_source as payment_source,
    'play' as product,
    case
      when play_trial_sku like "%lifetime%" then 'lifetime'
      when play_trial_sku like "%_12%" then 'yearly'
      when play_trial_sku like "%_1%" then 'monthly'
      when play_trial_sku like "%_6%" then 'semesterly'
      when play_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    "fts" as calc_type, 
    count(0) as total,
    cast(null as string) as payment_type
  from
   `celtic-music-240111.dbt_prod_base.ba_user_data` u
  group by
    u.user_id, 
    play_trial_start_date,
    play_ft_payment_source,
    product,
    plan_type

union all

   --Obtain all of the FTs that a user has had and also identify what product it is and plan type.
   -- only for LEARN
  select
    u.user_id, 
    learn_trial_start_date as date,
    learn_ft_payment_source as payment_source,
    'learn' as product,
    case
      when learn_trial_sku like "%lifetime%" then 'lifetime'
      when learn_trial_sku like "%_12%" then 'yearly'
      when learn_trial_sku like "%_1%" then 'monthly'
      when learn_trial_sku like "%_6%" then 'semesterly'
      when learn_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    "fts" as calc_type, 
    count(0) as total,
    cast(null as string) as payment_type
  from
    `celtic-music-240111.dbt_prod_base.ba_user_data` u
  group by
    u.user_id, 
    learn_trial_start_date,
    learn_ft_payment_source,
    product,
    plan_type
)

-- calc_type is a variable that will help us identify what the count is for
SELECT ut.calc_type, ut.date, ut.payment_source, u.os, u.network, u.country, u.kinedu_language, ut.product, ut.plan_type, ut.payment_type, 
SUM(ut.total) as total_count -- Obtain count by group
FROM `celtic-music-240111.dbt_prod_base.ba_user_data` u
join unioned_table ut on u.user_id = ut.user_id
GROUP BY 
ut.calc_type, ut.date, ut.payment_source, u.os, u.network, u.country, u.kinedu_language, ut.product, ut.plan_type, ut.payment_type
ORDER BY ut.calc_type, ut.date, ut.product

