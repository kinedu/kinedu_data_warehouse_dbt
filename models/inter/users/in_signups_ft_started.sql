with fts_union as (
  select
    signup_date as date,
    cast(null as string) as payment_source,
    os,
    network,
    country,
    kinedu_language,
    cast(null as string) as product,
    cast(null as string) as plan_type,
    count(0) as signups,
    null as fts_started
  from
    `celtic-music-240111.dbt_prod_base.ba_user_data`
  group by
    signup_date,
    payment_source,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    play_trial_start_date as date,
    play_ft_payment_source as payment_source,
    os,
    network,
    country,
    kinedu_language,
    'play' as product,
    case
      when play_trial_sku like "%lifetime%" then 'lifetime'
      when play_trial_sku like "%_12%" then 'yearly'
      when play_trial_sku like "%_1%" then 'monthly'
      when play_trial_sku like "%_6%" then 'semesterly'
      when play_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    count(0) as fts_started
  from
   `celtic-music-240111.dbt_prod_base.ba_user_data`
  group by
    play_trial_start_date,
    payment_source,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type

union all

  select
    learn_trial_start_date as date,
    learn_ft_payment_source as payment_source,
    os,
    network,
    country,
    kinedu_language,
    'learn' as product,
    case
      when learn_trial_sku like "%lifetime%" then 'lifetime'
      when learn_trial_sku like "%_12%" then 'yearly'
      when learn_trial_sku like "%_1%" then 'monthly'
      when learn_trial_sku like "%_6%" then 'semesterly'
      when learn_trial_sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    null as signups,
    count(0) as fts_started
  from
    `celtic-music-240111.dbt_prod_base.ba_user_data`
  group by
    learn_trial_start_date,
    payment_source,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type
)

select
  date,
  payment_source,
  os,
  network,
  country,
  kinedu_language,
  product,
  plan_type,
  sum(signups) as signups,
  sum(fts_started) as fts_started
from
  fts_union
group by
  date,
  payment_source,
  os,
  network,
  country,
  kinedu_language,
  product,
  plan_type