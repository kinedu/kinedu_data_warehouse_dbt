select
  date,
  os,
  network,
  country,
  kinedu_language,
  product,
  plan_type,
  sum(signups) as signups,
  sum(fts_started) as fts_started,
  sum(fts_converted) as fts_converted,
from
  {{ ref('in_signup_ft_conversion_date_counts_pre1')}}
group by
  date,
  os,
  network,
  country,
  kinedu_language,
  product,
  plan_type
