{{ config( materialized = "view") }}

with u as (
  select
    *
  from
    {{ ref('ba_user_data')}}
),

b as (
  select
    *
    except (account_id)
  from
    {{ ref('caf_babies_data')}}
),

pre_final as (
  select
    *,
    date_diff(u.signup_date, b.baby_birthday, day) as pre_baby_age_at_signup_days,
    /*date_diff(u.premium_conversion_date, b.baby_birthday, day) as baby_age_at_conversion_days,*/
    if(b.baby_id is null, 0, 1) as has_baby_registered
  from
    u
    left join b using (user_id)
),

final as (
  select
    if(pre_baby_age_at_signup_days < 0, 1, 0) signed_up_before_baby_birth,
    if(pre_baby_age_at_signup_days < 0, 0, pre_baby_age_at_signup_days) as baby_age_at_signup_days,
    *
    /*except(pre_baby_age_at_signup_days)*/
  from
    pre_final
)

select * from final
