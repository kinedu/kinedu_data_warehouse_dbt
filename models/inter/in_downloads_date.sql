select
    download_date as date,
    os,
    country,
    sum(downloads) as downloads
from 
    {{ ref('caf_app_downloads')}}
group by
    download_date,
    os,
    country