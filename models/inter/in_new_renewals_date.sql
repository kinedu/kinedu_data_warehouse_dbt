with aux as(
    select 
      sale_date as date,
      os,
      network,
      country,
      kinedu_language,
      product,
      plan_type,
      count(0) as new_subscriptions,
      cast(sum(usd_amount) as float64) as new_subscriptions_sales,
      null as renewals_yearly_ios,
      null as renewals_sales_yearly_ios,
      null as renewals,
      null as renewals_sales
    from
        {{ ref('in_first_sale_user')}}
    -- where 
    --     sku like '%premium%' 
    --     and payment_processor not like '%giftcard%'
    group by
        sale_date,
        os,
        network,
        country,
        kinedu_language,
        product,
        plan_type

    union all

    select 
      sale_date as date,
      os,
      network,
      country,
      kinedu_language,
      product,
      plan_type,
      null as new_subscriptions,
      null as new_subscriptions_sales,
      count(0) as renewals_yearly_ios,
      cast(sum(usd_amount) as float64) as renewals_sales_yearly_ios,
      null as renewals,
      null as renewals_sales
    from 
        {{ ref('in_track_subscriptions')}}
    where 
        new_subscription = 0 and num = 1 and (plan_status like "%recurring_yearly%" and os='iOS')
        #get only yearly renewals from iOS
    group by 
        sale_date,
        os,
        network,
        country,
        kinedu_language,
        product,
        plan_type

    union all

    select 
      sale_date as date,
      os,
      network,
      country,
      kinedu_language,
      product,
      plan_type,
      null as new_subscriptions,
      null as new_subscriptions_sales,
      null as renewals_yearly_ios,
      null as renewals_sales_yearly_ios,
      count(0) as renewals,
      cast(sum(usd_amount) as float64) as renewals_sales
    from 
        {{ ref('in_track_subscriptions')}}
    where 
        new_subscription = 0 and num = 1 and not (plan_status like "%recurring_yearly%" and os='iOS')
        # get all the renewals except yearly from iOS
    group by 
        sale_date,
        os,
        network,
        country,
        kinedu_language,
        product,
        plan_type
)

select
    date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type,
    sum(new_subscriptions) as new_subscriptions,
    sum(new_subscriptions_sales) as new_subscriptions_sales,
    sum(renewals_yearly_ios) as renewals_yearly_ios,
    sum(renewals_sales_yearly_ios) as renewals_sales_yearly_ios,
    sum(renewals) as renewals,
    sum(renewals_sales) as renewals_sales
from
    aux
group by
    date,
    os,
    network,
    country,
    kinedu_language,
    product,
    plan_type