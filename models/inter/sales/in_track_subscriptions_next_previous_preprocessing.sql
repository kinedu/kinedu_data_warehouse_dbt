{{ config( materialized = "table") }}

with tspp as (
  select * from {{ ref('pre_inter_track_subscriptions_preprocessing') }}
)

  select
    tspp.*,
    CONCAT(CAST(EXTRACT(YEAR from tspp.service_date) as string), "-", LPAD(CAST(EXTRACT(MONTH from tspp.service_date) as string),2,'0')) as service_date_month_key,
    ts_next.service_date next_service_date,
    ts_previous.service_date as previous_service_date,
    /* if(tspp.plan_type <> ts_previous.plan_type, 1, 0) as changed_plan,*/
    case
      when
        tspp.plan_type <> ts_previous.plan_type
      then 1
      else 0
    end as changed_plan,
    case
    /* The following notes you will find inside the code (except for the big block a few lines below ) are experiments  that need review and solution for they're supposed to improve the reliability of this model and will make it easier to interprete */ 
      when
         /*tspp.user_id = ts_previous.user_id  and
         (*/
        tspp.plan_type = ts_previous.plan_type /*
         )*/
      then "same_plan"
      when
       /* tspp.user_id = ts_previous.user_id  and
        (*/
        (tspp.plan_type = 'lifetime' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'semesterly' or ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'yearly' and (ts_previous.plan_type = 'semesterly' or ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'semesterly' and (ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'quarterly' and (ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'monthly' and (ts_previous.plan_type = 'other')) /*
        ) */
      then "upgrade"
      when
        /* tspp.user_id = ts_previous.user_id  and 
        ( */
        (tspp.plan_type = 'other' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'semesterly' or ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly'))/* changed other to monthly, status: approved */
        or (tspp.plan_type = 'monthly' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'semesterly' or ts_previous.plan_type = 'quarterly'))
        or (tspp.plan_type = 'quarterly' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'semesterly'))
        or (tspp.plan_type = 'semesterly' and (ts_previous.plan_type = 'yearly')) /*
        ) */
      then "downgrade"
      else null
    end as plan_change,

    /* if(tspp.plan_type <> ts_next.plan_type, 1, 0) as next_row_changes_plan,*/
    case
      when
        tspp.plan_type <> ts_next.plan_type
      then 1
      else 0
    end as next_row_changes_plan,
    /* if(tspp.user_id = ts_next.user_id, 1, 0) as same_user_as_next_row,*/
    case
      when
        tspp.user_id = ts_next.user_id
      then 1
      else 0
    end as same_user_as_next_row,
    /* if(tspp.user_id = ts_previous.user_id, 1, 0) as same_user_as_previous_row,*/
    case
      when
        tspp.user_id = ts_previous.user_id
      then 1
      else 0
    end as same_user_as_previous_row,    
    12 * (extract(year from ts_next.service_date) - extract(year from tspp.service_date)) + (extract(month from ts_next.service_date) - extract(month from tspp.service_date)) as month_diff_to_next_row,
    12 * (extract(year from tspp.service_date) - extract(year from ts_previous.service_date)) + (extract(month from tspp.service_date) - extract(month from ts_previous.service_date)) as month_diff_to_previous_row
  from tspp
    left join tspp as ts_next on tspp.inc_id + 1 = ts_next.inc_id
    left join tspp as ts_previous on tspp.inc_id - 1 = ts_previous.inc_id
  order by
    tspp.inc_id