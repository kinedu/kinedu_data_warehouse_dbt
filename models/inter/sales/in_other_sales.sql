--Stripe sales (masterclass/coaching sessions)

select 
sum(amount/100) as other_sales,
count(0) as other_purchases,
cast(timestamp_seconds(created) as date) as date,
'stripe' as network
from `celtic-music-240111.stripe_leads_import.charge` 
where refunded is false
and status='succeeded'
group by cast(timestamp_seconds(created) as date), network

union all 

select 
sum(amount/100) as other_sales, 
count(0) as other_purchases,
cast(timestamp_seconds(created) as date) as date,
'stripe' as network
from `celtic-music-240111.stripe_ww_mx_import.charge`
where refunded is false and status='succeeded'
and (description is null or description not like '%Subscription%')
group by cast(timestamp_seconds(created) as date), network

union all

select 
sum((amount/100)/5) as other_sales, --conversion de reales a usd 5reales=1usd
count(0) as other_purchases,
cast(timestamp_seconds(created) as date) as date,
'stripe' as network
from `celtic-music-240111.stripe_brazil_import.charge` 
where refunded is false
and status='succeeded'
and (description is null or description not like '%Subscription%')
group by cast(timestamp_seconds(created) as date), network

--Shopify sales mx (toys)
union all

select 
sum(total_price/19.5) as other_sales, --conversion de pesos a usd 
count(0) as other_purchases,
date(created_at) as date,
'shopify' as network
from `celtic-music-240111.shopify_kinedumx_import.orders`
where financial_status='paid'
group by date(created_at), network