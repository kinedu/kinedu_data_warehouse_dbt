with final as (
    select 
        id,
        sale_id,
        sale_date,     
        user_id,      
        usd_amount, 
        payment_source,
        payment_processor,
        renewed_automatically,
        sku,
        network,
        country,
        kinedu_language,
        campaign_name,
        signup_date,
        os,
        premium_conversion_date,
        product,
        plan_type
    from 
        {{ ref('in_track_subscriptions')}} 
    where 
        new_subscription = 1 
)

select * from final