{{ config( materialized = "table") }}

with ts_next_previous as (
  select * from {{ ref('in_track_subscriptions_next_previous_preprocessing') }}
),

ts_labeled as (
  select
    *,
    /*if(same_user_as_previous_row = 0, 1, 0) as new_paying_user,*/
    case
      when
        same_user_as_previous_row = 0
      then 1
      else 0
    end as new_paying_user,
    /*if(same_user_as_previous_row = 1 and plan_change = 'upgrade', 1, 0) as upsell,*/
    case
      when
        same_user_as_previous_row = 1 and plan_change = 'upgrade'
      then 1
      else 0
    end as upsell,
    /*if(same_user_as_previous_row = 1 and plan_change = 'downgrade', 1, 0) as downgrade,*/
    case
      when
        same_user_as_previous_row = 1 and plan_change = 'downgrade'
      then 1
      else 0
    end as downgrade,
    /*if(same_user_as_previous_row = 1 and month_diff_to_previous_row <= 1 and plan_change = 'same_plan', 1, 0) as recurring,*/
    case
      when
        same_user_as_previous_row = 1 and month_diff_to_previous_row <= 1 and plan_change = 'same_plan'
      then 1
      else 0
    end as recurring,
    /*if(same_user_as_previous_row = 1 and month_diff_to_previous_row > 1 and plan_change = 'same_plan', 1, 0) as winback,*/
    case
      when
        same_user_as_previous_row = 1 and month_diff_to_previous_row > 1 and plan_change = 'same_plan'
      then 1
      else 0
    end as winback,
    /*if((same_user_as_next_row = 0 and plan_type <> 'lifetime') or (same_user_as_next_row = 1 and month_diff_to_next_row > 1), 1, 0) as cancellation_next_month*/
    case
      when
        (same_user_as_next_row = 0 and plan_type <> 'lifetime') or (same_user_as_next_row = 1 and month_diff_to_next_row > 1)
      then 1
      else 0
    end as cancellation_next_month    
  from
    ts_next_previous
),

ts_plan_labeled_new as (
  select
    *,
    cast(usd_amount as float64) as paid_usd_amount,
    cast(distributed_amount as float64) as dist_amount,
    case
      when plan_type = 'other' and new_paying_user = 1 then "new_paying_user_other"
      when plan_type = 'other' and downgrade = 1 then "downgrade_to_other"
      when plan_type = 'other' and recurring = 1 then "recurring_other"
      when plan_type = 'other' and winback = 1 then "winback_other"

      when plan_type = 'monthly' and new_paying_user = 1 then "new_paying_user_monthly"
      when plan_type = 'monthly' and downgrade = 1 then "downgrade_to_monthly"
      when plan_type = 'monthly' and upsell = 1 then "upsell_to_monthly"
      when plan_type = 'monthly' and recurring = 1 then "recurring_monthly"
      when plan_type = 'monthly' and winback = 1 then "winback_monthly"

      when plan_type = 'quarterly' and new_paying_user = 1 then "new_paying_user_quarterly"
      when plan_type = 'quarterly' and downgrade = 1 then "downgrade_to_quarterly"
      when plan_type = 'quarterly' and upsell = 1 then "upsell_to_quarterly"
      when plan_type = 'quarterly' and recurring = 1 then "recurring_quarterly"
      when plan_type = 'quarterly' and winback = 1 then "winback_quarterly"

      when plan_type = 'semesterly' and new_paying_user = 1 then "new_paying_user_semesterly"
      when plan_type = 'semesterly' and downgrade = 1 then "downgrade_to_semesterly"
      when plan_type = 'semesterly' and upsell = 1 then "upsell_to_semesterly"
      when plan_type = 'semesterly' and recurring = 1 then "recurring_semesterly"
      when plan_type = 'semesterly' and winback = 1 then "winback_semesterly"

      when plan_type = 'yearly' and new_paying_user = 1 then "new_paying_user_yearly"
      when plan_type = 'yearly' and upsell = 1 then "upsell_to_yearly"
      when plan_type = 'yearly' and recurring = 1 then "recurring_yearly"
      when plan_type = 'yearly' and winback = 1 then "winback_yearly"

      when plan_type = 'lifetime' and new_paying_user = 1 then "new_paying_user_lifetime"
      when plan_type = 'lifetime' and upsell = 1 then "upsell_to_lifetime"
      else null
    end as plan_status,

    /*if(plan_type = 'monthly' and cancellation_next_month = 1, 1, 0) as monthly_cancellation_next_month,*/
    case
      when
        plan_type = 'monthly' and cancellation_next_month = 1
      then 1
      else 0
    end as monthly_cancellation_next_month,
    /*if(plan_type = 'quarterly' and cancellation_next_month = 1, 1, 0) as quarterly_cancellation_next_month,*/
    case
      when
        plan_type = 'quarterly' and cancellation_next_month = 1
      then 1
      else 0
    end as quarterly_cancellation_next_month,
    /*if(plan_type = 'semesterly' and cancellation_next_month = 1, 1, 0) as semesterly_cancellation_next_month,*/
    case
      when
        plan_type = 'semesterly' and cancellation_next_month = 1
      then 1
      else 0
    end as semesterly_cancellation_next_month,
    /*if(plan_type = 'yearly' and cancellation_next_month = 1, 1, 0) as yearly_cancellation_next_month,*/
    case
      when
        plan_type = 'yearly' and cancellation_next_month = 1
      then 1
      else 0
    end as yearly_cancellation_next_month,
    /*if(plan_type = 'other' and cancellation_next_month = 1, 1, 0) as other_cancellation_next_month*/
    case
      when
        plan_type = 'other' and cancellation_next_month = 1
      then 1
      else 0
    end as other_cancellation_next_month    
  from
    ts_labeled
),

ts_reordered as (
  select
    *,
    ROW_NUMBER() OVER() AS ts_id
  from
    ts_plan_labeled_new
),

final as (
  select
    ts.*,
    if(ynu.plan_status = 'new_paying_user_yearly' or ynu.plan_status = 'upsell_to_yearly', 1, 0) as is_first_year_final_month,
    if(mnu.plan_status = 'new_paying_user_monthly' /*and mnu.user_id = ts.user_id */, 1, 0) as is_second_month
  from
    ts_reordered ts
    left join ts_reordered ynu on ts.ts_id - 11 = ynu.ts_id
    left join ts_reordered mnu on ts.ts_id - 1 = mnu.ts_id
)

select * from final