with refunds_all as (
	select
		refund_id,
		sale_id,
		charge_id,
		refund_datetime,
		refund_date,
		ROW_NUMBER() OVER (PARTITION BY sale_id ORDER BY refund_id) AS row_num
	from
		{{ref('caf_refunds')}}
),

refunds as (
	select
		refund_id,
		sale_id,
		charge_id,
		refund_datetime,
		refund_date,
		row_num
	from
		refunds_all
    where
        row_num = 1
),
-- sales as (
-- 	select
-- 		sale_id, 
-- 		user_id,
-- 		usd_amount, 
-- 		-- month_key, 
-- 		-- renewed_automatically, 
-- 		plan_type, 
-- 		product
-- 		-- payment_processor, 
-- 		-- is_partner_sku
-- 	from
-- 		{{ref('in_track_subscriptions')}}
-- )

sales as (
	select
		sale_id, 
		user_id,
		usd_amount, 
		-- month_key, 
		-- renewed_automatically, 
		plan_type, 
		sale_date,
		case 
			when sku like "%learn%" then "learn"
			else "play"
		end as product
		-- payment_processor, 
		-- is_partner_sku
	from
		{{ ref('caf_sales_all') }}
),

users as (
	select
		user_id,
		signup_date,
		os,
		network,
		country,
		kinedu_language,
		-- if a user has 2 ft conversion date, it will only count the latest. Past analytics may change if play is sold again.
		nullif(GREATEST(COALESCE(play_trial_converted_date, '0001-01-01'), COALESCE(learn_trial_converted_date, '0001-01-01')), '0001-01-01') as latest_trial_conv_date
	from
		{{ ref('ba_user_data')}}
),

refunds_users as (
	select
		refunds.refund_id,
		refunds.sale_id,
		refunds.refund_datetime,
		refunds.refund_date,
		sales.usd_amount,
		sales.plan_type,
		sales.product,
		sales.sale_date,
		users.user_id,
		users.os,
		users.network,
		users.country,
		users.kinedu_language,
		case 
			when sales.sale_date = users.latest_trial_conv_date then true
		else false end as refund_was_ft_converted
	from
		refunds 
		join sales using (sale_id)
		join users using (user_id)
),

refunds_per_sale_date as (
	select	
		sale_date as date,
		os,
		network,
		country,
		kinedu_language,
		product,
		plan_type,
		refund_was_ft_converted,
		"sale_date" as date_type,
		sum(usd_amount) as refunds_total_amount,
		count(distinct sale_id) as num_of_refunds
	from
		refunds_users
	group by
		date,
		sale_id,
		os,
		network,
		country,
		kinedu_language,
		product,
		plan_type,
		refund_was_ft_converted
),

refunds_per_refund_date as (
	select	
		refund_date as date,
		os,
		network,
		country,
		kinedu_language,
		product,
		plan_type,
		refund_was_ft_converted,
		"refund_date" as date_type,
		sum(usd_amount) as refunds_total_amount,
		count(distinct sale_id) as num_of_refunds
	from
		refunds_users
	group by
		date,
		sale_id,
		os,
		network,
		country,
		kinedu_language,
		product,
		plan_type,
		refund_was_ft_converted
)

select 
	date,
	os,
	network,
	country,
	kinedu_language,
	product,
	plan_type,
	date_type,
	refund_was_ft_converted,
	refunds_total_amount,
	num_of_refunds
from 
	refunds_per_sale_date

union all

select
	date,
	os,
	network,
	country,
	kinedu_language,
	product,
	plan_type,
	date_type,
	refund_was_ft_converted, 
	refunds_total_amount,
	num_of_refunds
from
    refunds_per_refund_date

