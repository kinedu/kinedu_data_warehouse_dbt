{{ config( materialized = "table") }}

with ts_union as (
  select
    user_id,
    sale_date,
    id,
    sale_id,
    payment_processor,
    payment_source,
    sku,
    usd_amount,
    renewed_automatically,
    month_key,
    plan_type,
    is_partner_sku,
    code_name, 
    code, 
    service_months,
    num,
    distributed_amount,
    service_date,
    inc_id,
    service_date_month_key,
    next_service_date,
    previous_service_date,
    changed_plan,
    plan_change,
    next_row_changes_plan,
    same_user_as_next_row,
    same_user_as_previous_row,
    month_diff_to_next_row,
    month_diff_to_previous_row,
    new_paying_user,
    upsell,
    downgrade,
    recurring,
    winback,
    cancellation_next_month,

    null as new_paying_user_learn,
    null as upsell_learn,
    null as downgrade_learn,
    null as recurring_learn,
    null as winback_learn,
    null as cancellation_next_month_learn,

    paid_usd_amount,
    dist_amount,
    plan_status,
    monthly_cancellation_next_month,
    quarterly_cancellation_next_month,
    semesterly_cancellation_next_month,
    yearly_cancellation_next_month,
    other_cancellation_next_month,

    null as monthly_cancellation_next_month_learn,
    null as quarterly_cancellation_next_month_learn,
    null as yearly_cancellation_next_month_learn,
    null as other_cancellation_next_month_learn,

    ts_id,
    is_first_year_final_month,
    is_second_month,
    "play" as product
  from
    {{ ref('in_track_subscriptions_preprocessing') }}

union all 

  select
    user_id,
    sale_date,
    id,
    sale_id,
    payment_processor,
    payment_source,
    sku,
    usd_amount,
    renewed_automatically,
    month_key,
    plan_type,
    is_partner_sku,
    code_name, 
    code, 
    service_months,
    num,
    distributed_amount,
    service_date,
    inc_id,
    service_date_month_key,
    next_service_date,
    previous_service_date,
    changed_plan,
    plan_change,
    next_row_changes_plan,
    same_user_as_next_row,
    same_user_as_previous_row,
    month_diff_to_next_row,
    month_diff_to_previous_row,

    null as new_paying_user,
    null as upsell,
    null as downgrade,
    null as recurring,
    null as winback,
    null as cancellation_next_month,

    new_paying_user_learn,
    upsell_learn,
    downgrade_learn,
    recurring_learn,
    winback_learn,
    cancellation_next_month_learn,

    paid_usd_amount,
    dist_amount,
    plan_status,

    null as monthly_cancellation_next_month,
    null as quarterly_cancellation_next_month,
    null as semesterly_cancellation_next_month,
    null as yearly_cancellation_next_month,
    null as other_cancellation_next_month,

    monthly_cancellation_next_month_learn,
    quarterly_cancellation_next_month_learn,
    yearly_cancellation_next_month_learn,
    other_cancellation_next_month_learn,

    ts_id,
    is_first_year_final_month,
    is_second_month,
    "learn" as product
  from
    {{ ref('in_track_subscriptions_preprocessing_learn') }}
),

u as (
  select
    user_id,
    os,
    country,
    region,
    city,
    kinedu_region,
    signup_date,
    network,
    campaign_name,
    user_gender,
    kinedu_language,
    premium_conversion_date,
    auto_renew_status
  from
    {{ ref('ba_user_data') }}
),

final as (
  select
    *,
    case when new_paying_user = 1 or new_paying_user_learn = 1 then 1 else 0 end as new_subscription
  from
    ts_union
    join u using (user_id)
)


select * from final
