{{ config( materialized = "table") }}

with tspp as (
  select * from {{ ref('pre_inter_track_subscriptions_preprocessing_learn') }}
),

ts_next_previous as (
  select
    tspp.*,
    CONCAT(CAST(EXTRACT(YEAR from tspp.service_date) as string), "-", LPAD(CAST(EXTRACT(MONTH from tspp.service_date) as string),2,'0')) as service_date_month_key,
    ts_next.service_date next_service_date,
    ts_previous.service_date as previous_service_date,
    if(tspp.plan_type <> ts_previous.plan_type, 1, 0) as changed_plan,

    case
    /* The following notes you will find inside the code (except for the big block a few lines below ) are experiments  that need review and solution for they're supposed to improve the reliability of this model and will make it easier to interprete */ 
      when
         /*tspp.user_id = ts_previous.user_id  and
         (*/
        tspp.plan_type = ts_previous.plan_type /*
         )*/
      then "same_plan"
      when
       /* tspp.user_id = ts_previous.user_id  and
        (*/
        (tspp.plan_type = 'yearly' and (ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'quarterly' and (ts_previous.plan_type = 'monthly' or ts_previous.plan_type = 'other'))
        or (tspp.plan_type = 'monthly' and (ts_previous.plan_type = 'other')) /*
        ) */
      then "upgrade"
      when
        /* tspp.user_id = ts_previous.user_id  and 
        ( */
        (tspp.plan_type = 'other' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'quarterly' or ts_previous.plan_type = 'monthly'))/* changed other to monthly, status: approved */
        or (tspp.plan_type = 'monthly' and (ts_previous.plan_type = 'yearly' or ts_previous.plan_type = 'quarterly'))
        or (tspp.plan_type = 'quarterly' and (ts_previous.plan_type = 'yearly'))/*
        ) */
      then "downgrade"
      else null
    end as plan_change,

    if(tspp.plan_type <> ts_next.plan_type, 1, 0) as next_row_changes_plan,
    if(tspp.user_id = ts_next.user_id, 1, 0) as same_user_as_next_row,
    if(tspp.user_id = ts_previous.user_id, 1, 0) as same_user_as_previous_row,
    12 * (extract(year from ts_next.service_date) - extract(year from tspp.service_date)) + (extract(month from ts_next.service_date) - extract(month from tspp.service_date)) as month_diff_to_next_row,
    12 * (extract(year from tspp.service_date) - extract(year from ts_previous.service_date)) + (extract(month from tspp.service_date) - extract(month from ts_previous.service_date)) as month_diff_to_previous_row
  from
    tspp
    left join tspp as ts_next on tspp.inc_id + 1 = ts_next.inc_id
    left join tspp as ts_previous on tspp.inc_id - 1 = ts_previous.inc_id
  order by
    tspp.inc_id
),

ts_labeled as (
  select
    *,
    if(same_user_as_previous_row = 0, 1, 0) as new_paying_user_learn,
    if(same_user_as_previous_row = 1 and plan_change = 'upgrade', 1, 0) as upsell_learn,
    if(same_user_as_previous_row = 1 and plan_change = 'downgrade', 1, 0) as downgrade_learn,
    if(same_user_as_previous_row = 1 and month_diff_to_previous_row <= 1 and plan_change = 'same_plan', 1, 0) as recurring_learn,
    if(same_user_as_previous_row = 1 and month_diff_to_previous_row > 1 and plan_change = 'same_plan', 1, 0) as winback_learn,
    if((same_user_as_next_row = 0 and plan_type <> 'lifetime') or (same_user_as_next_row = 1 and month_diff_to_next_row > 1), 1, 0) as cancellation_next_month_learn
  from
    ts_next_previous
),


ts_plan_labeled_new as (
  select
    *,
    cast(usd_amount as float64) as paid_usd_amount,
    cast(distributed_amount as float64) as dist_amount,
    case
      when plan_type = 'other' and new_paying_user_learn = 1 then "new_paying_user_other_learn"
      when plan_type = 'other' and downgrade_learn = 1 then "downgrade_to_other_learn"
      when plan_type = 'other' and recurring_learn = 1 then "recurring_other_learn"
      when plan_type = 'other' and winback_learn = 1 then "winback_other_learn"

      when plan_type = 'monthly' and new_paying_user_learn = 1 then "new_paying_user_monthly_learn"
      when plan_type = 'monthly' and downgrade_learn = 1 then "downgrade_to_monthly_learn"
      when plan_type = 'monthly' and upsell_learn = 1 then "upsell_to_monthly_learn"
      when plan_type = 'monthly' and recurring_learn = 1 then "recurring_monthly_learn"
      when plan_type = 'monthly' and winback_learn = 1 then "winback_monthly_learn"

      when plan_type = 'quarterly' and new_paying_user_learn = 1 then "new_paying_user_quarterly_learn"
      when plan_type = 'quarterly' and downgrade_learn = 1 then "downgrade_to_quarterly_learn"
      when plan_type = 'quarterly' and upsell_learn = 1 then "upsell_to_quarterly_learn"
      when plan_type = 'quarterly' and recurring_learn = 1 then "recurring_quarterly_learn"
      when plan_type = 'quarterly' and winback_learn = 1 then "winback_quarterly_learn"

      when plan_type = 'yearly' and new_paying_user_learn = 1 then "new_paying_user_yearly_learn"
      when plan_type = 'yearly' and upsell_learn = 1 then "upsell_to_yearly_learn"
      when plan_type = 'yearly' and recurring_learn = 1 then "recurring_yearly_learn"
      when plan_type = 'yearly' and winback_learn = 1 then "winback_yearly_learn"

      else null
    end as plan_status,

    if(plan_type = 'monthly' and cancellation_next_month_learn = 1, 1, 0) as monthly_cancellation_next_month_learn,
    if(plan_type = 'quarterly' and cancellation_next_month_learn = 1, 1, 0) as quarterly_cancellation_next_month_learn,
    if(plan_type = 'yearly' and cancellation_next_month_learn = 1, 1, 0) as yearly_cancellation_next_month_learn,
    if(plan_type = 'other' and cancellation_next_month_learn = 1, 1, 0) as other_cancellation_next_month_learn
  from
    ts_labeled
),


ts_reordered as (
  select
    *,
    ROW_NUMBER() OVER() AS ts_id
  from
    ts_plan_labeled_new
),

final as (
  select
    ts.*,
    if(ynu.plan_status = 'new_paying_user_yearly_learn' or ynu.plan_status = 'upsell_to_yearly_learn', 1, 0) as is_first_year_final_month,
    if(mnu.plan_status = 'new_paying_user_monthly_learn' /*and mnu.user_id = ts.user_id */, 1, 0) as is_second_month
  from
    ts_reordered ts
    left join ts_reordered ynu on ts.ts_id - 11 = ynu.ts_id
    left join ts_reordered mnu on ts.ts_id - 1 = mnu.ts_id
)


select * from final
