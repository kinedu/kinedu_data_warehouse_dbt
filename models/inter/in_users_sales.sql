with u as (
    select
        user_id,
        network,
        campaign_name,
        signup_date,
        cancellation_date,
        email,
        mp_version,
        os,
        premium_conversion_date,
        country
    from
        {{ ref('ba_user_data')}}
),
s as (
    select
        sale_id,
        sku,
        plan_type,
        status,
        usd_amount,
        user_id,
        renewed_automatically
    from
        {{ ref('ba_sales')}}
    
),

final as (
    select 
        *
    from
        u right join s
        using(user_id)
)

select * from final
 