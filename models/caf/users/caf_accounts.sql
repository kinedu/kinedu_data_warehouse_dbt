with cte
as (
  select
    a.id as account_id,
    a.owner_id as owner_id,
    a.plan_key,
    a.plan_status as auto_renew_status,
    a.plan_expires_on,
    a.plan_expired_on,
    row_number() over (partition by a.owner_id order by a.id) as first_record
  from
    `celtic-music-240111.aws_kinedu_app_import.accounts` a
  where
    a.__hevo__marked_deleted is false
)

select 
  account_id,
  owner_id,
  plan_key,
  auto_renew_status,
  plan_expires_on,
  plan_expired_on
from 
  cte
where 
  first_record = 1