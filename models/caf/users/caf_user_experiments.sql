select
    ue.experiment_type,
    ue.experiment_name,
    ue.user_id,
    date(ue.created_at) as user_joined_experiment_date
from
    `celtic-music-240111.aws_kinedu_app_import.user_experiments` ue
where
    ue.__hevo__marked_deleted is false

