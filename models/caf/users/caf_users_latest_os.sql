{{ config( materialized = "table") }}


with te as (
  SELECT
    user_id,
    os
  FROM 
   {{ ref('caf_users') }}
)

select user_id, os from te 