with cte  
as  
  (  
    select
      uxd.id as uxd_id,
      uxd.user_id as user_id,
      uxd.ad_google_network_subtype,
      uxd.ad_google_property,
      uxd.ad_keyword,
      date(uxd.cancellation_date) as cancellation_date,
      date(uxd.trial_start) as trial_start_date,
      date(uxd.trial_converted) as trial_converted_date,
      date(uxd.premium_conversion_date) as premium_conversion_date,
      premium_conversion_charge,
      trial_charge,
      row_number() over (partition by uxd.user_id order by uxd.id) as first_record -- for elimination of duplicated data.
    from
      `celtic-music-240111.aws_kinedu_app_import.user_extra_data` uxd
      inner join   {{ ref('caf_users')}} as u
      on u.user_id=uxd.user_id
    where
      uxd.__hevo__marked_deleted is false
  )

select 
      uxd_id,
      user_id,
      ad_google_network_subtype,
      ad_google_property,
      ad_keyword,
      cancellation_date,
      trial_start_date,
      trial_converted_date,
      premium_conversion_date,
      premium_conversion_charge,
      trial_charge
from 
  cte
where 
first_record = 1