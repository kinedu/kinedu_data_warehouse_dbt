{{ config( materialized = "table") }}
with u as (
  select
    u.signup_provider,
    u.id as user_id,
    case
      when u.mp_os in ("iOS", 'iPad', 'iPhone', 'iOS 8', 'iOS 9', 'tvOS','watchOS') then "iOS"
      when u.mp_os in ("Mac", "Windows" , "Linux", "Mac OS X", "Chrome OS") then "Web"
      when u.mp_os in (""  ,"Unknown") or u.mp_os is null then "Unknown"
      when u.mp_os in ("android","Windows Phone", "BlackBerry") then "Android"
      else u.mp_os
    end as os,
    case 
        when u.mp_country is null or u.mp_country = '' or u.mp_country = ' ' then 'Unknown'
        else u.mp_country
    end as country,
    u.mp_region as region,
    u.mp_city as city,
    u.mp_timezone as timezone,
    date(u.created_at) as signup_date,
    u.created_at,
    case
      when u.adjust_network = '' or u.adjust_network = 'Organic' or u.adjust_network = 'organic' or u.adjust_network is null then 'Organics'
      when u.adjust_network like '%Facebook%' or u.adjust_network like '%Instagram%' or u.adjust_network = 'Restricted' then 'Facebook'
      when u.adjust_network like '%Pinterest%' then 'Pinterest'
      when u.adjust_network like '%Google%' then 'Google'
      when u.adjust_network like '%SmartLinks%' or u.adjust_network like '%smartlinks%' or u.adjust_network like '%Smartlinks%' then 'Smartlinks'
      when u.adjust_network like '%Smadex%' then 'Smadex'
      when u.adjust_network like '%Twitter%' then 'Twitter'
      when u.adjust_network like '%Infleux%' then 'Infleux'
      when u.adjust_network like '%MoBrain%' then 'MoBrain'
      when u.adjust_network like '%Fyber%' then 'Fyber'
      when u.adjust_network like '%Apple Search Ads%' then 'Apple Search Ads'
      when u.adjust_network like '%RankMyApp%' then 'RankMyApp'
      when u.adjust_network like '%GlobalWide Media%' then 'GlobalWide Media'
      else u.adjust_network
    end as network,
    case
      when
        u.adjust_network like '%Facebook%'
        or u.adjust_network like '%Instagram%'
        or u.adjust_network like '%Apple Search Ads%'
        or u.adjust_network like '%Google%'
      then u.adjust_campaign
      when u.adjust_network like '%Pinterest%' then substr(u.adjust_campaign,1,(length(regexp_extract(u.adjust_campaign, r'^(.*)\|.*'))))
    end as campaign_name,
    u.adjust_campaign,
    u.adjust_adgroup as adset_name,
    u.adjust_creative as ad_name,
    CAST(u.birthday AS timestamp ) as birthday,
    u.device,
    u.gender as user_gender,
    u.mp_version,
    u.mp_app,
    u.email,
    u.name as user_name,  
    u.lastname as user_lastname,
    u.ref as experience_type,
    case
      when u.locale in ('es-MX', 'es') then 'es'
      when u.locale = 'pt' then 'pt'
      when u.locale = 'en' then 'en'
    else 'en' end as kinedu_language
  from
    `celtic-music-240111.aws_kinedu_app_import.users` u
  /* usuarios originados por el ataque del 31 de Enero del 2023 */ 
  left join `celtic-music-240111.dbt_prod_caf.caf_blacklist_users` black on black.user_id = u.id
  where
    u.__hevo__marked_deleted is false
    and black.user_id is null
    /* usuarios de prueba, back nos dio la lista */
    and u.name not like "%click here%"
    and u.name not like "%open site%"
    and u.email not like "%@test.com%"
    AND u.email NOT LIKE '%missing+%' 
    AND u.email not LIKE '%@prueba%'
    AND u.email NOT LIKE '%+prueba%'
    AND u.email NOT LIKE '%@example.com%'
    AND u.email NOT LIKE '%+test%'
    AND u.email NOT LIKE '%@cool.com%'
    AND u.email NOT LIKE '%_test%'
    AND u.email NOT LIKE 'test@%'
    AND u.email NOT LIKE 'ana.cris.tovar%@%'
    AND u.email NOT LIKE '%diego%@growee%'
    AND u.email NOT LIKE '%lelizondo23%'
    AND u.email NOT IN ('gfarias@gmail.com',
                                        'ana.cris.tovar@gmail.com',
                                        'anacristina@kinedu.com',
                                        '03@kinedu.com',
                                        'checotrevino-test01@gmail.com',
                                        'deleted--checotrevino-test02@gmail.com',
                                        'mariana.tguajardo@gmail.com',
                                        'deleted--checotrevino-test03@gmail.com',
                                        'mariana.tguajardo@gmail.com',
                                        'checotrevino-test05@gmail.com',
                                        'ana.cris.tovar+test1@gmail.com',
                                        'diegomarcos@outlook.com',
                                        'aliceravize@hotmail.com',
                                        'checotrevino+2012@gmail.com',
                                        'sleal9@gmail.com',
                                        'jegv91@gmail.com',
                                        'alexdelarosacortes@gmail.com',
                                        'jijij@kdjfkfjf.con',
                                        'kokok@kokohdjdjdjd.com',
                                        'jorgegilcavazos@gmail.com',
                                        '39vdtrd3vf@privaterelay.appleid.com',
                                        'anasofia.guerra@gmail.com',
                                        'ana_brtvyys_t@tfbnw.net',
                                        'bjgwmjpm7f@privaterelay.appleid.com',
                                        'djksmshsjq@jdkd.com',
                                        'flyflyerson4@gmail.com',
                                        'ghernandez.9002@gmail.com',
                                        'gisellapr@gmail.com',
                                        'isabelle.delgado88@gmail.com',
                                        'kptpjgn_wisemanwitz_1444144220@tfbnw.net',
                                        'lalo_sa_94@hotmail.com',
                                        'moni_garcia79@hotmail.com',
                                        'no-email@kinedu.com',
                                        'nsnsjsja@teat.com',
                                        'opo427@tst.com',
                                        'vanwykbeate@gmail.com',
                                        '8gvp4e7443@privaterelay.appleid.com')

)
select * from u