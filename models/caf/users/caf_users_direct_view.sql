{{ config( materialized = "view") }}


SELECT 
    user_id,
    mp_plan 
FROM (
	SELECT 
        u.id as user_id,
        case
			when plan_key like "%play%" then 'play'
			when plan_key like "%learn%" then 'learn'
			when plan_key like "%thrive%" then 'thrive'
			when plan_key like "%premium_license%" then 'premium_license'
			when plan_key like "%classroom_premium%" then 'classroom_premium'
			when plan_key like "%free%" then 'freemium'
			when plan_key like "%premium%" then 'premium'
		else plan_key
		end as mp_plan,
        row_number() over (partition by u.id order by a.plan_updated_at desc) as last_updated_plan --tomar el plan más reciente
    FROM
        `celtic-music-240111.aws_kinedu_app_import.users` u
        join `celtic-music-240111.aws_kinedu_app_import.memberships` m on u.id = m.user_id
        join `celtic-music-240111.aws_kinedu_app_import.accounts` a on a.id = m.account_id
        ) 
 WHERE last_updated_plan=1


	/*
play, learn, classroom_premium, thrive and premium_license
	*/