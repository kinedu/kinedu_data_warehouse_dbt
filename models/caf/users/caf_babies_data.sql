/*TODO: add which is the user's first baby*/
select
	b.id as baby_id,
	b.birthday as baby_birthday,
	DATE(b.created_at) as baby_creation_date,
	b.gender as baby_gender,
	b.author_id as user_id,
	b.name as baby_name,
	b.account_id,
	b.weeks_before_birth
from
	`celtic-music-240111.aws_kinedu_app_import.babies` b
where
	__hevo__marked_deleted = false
