{{ config( materialized = "view") }}


select
	id as membership_id,
	user_id,
	account_id,
	created_at as membership_created_at,
	status,
	new_user_data
from
	`celtic-music-240111.aws_kinedu_app_import.memberships`
where 
     __hevo__marked_deleted is false