select 
    id as prospect_id,
   CAST(created_at AS timestamp ) as created_at,
    email as prospect_email,
    name as prospect_name,
    referrer_id
from
	`celtic-music-240111.aws_kinedu_app_import.classrooms_prospect_users` 
where 
     __hevo__marked_deleted is false