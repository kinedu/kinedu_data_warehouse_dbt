{{ config( materialized = "table") }}

with u as (
    select
        user_id
    from
       {{ ref('caf_users') }}
),

c as (
    select
        c.id as charge_id,
        c.user_id,
        c.payment_source as payment_source,
        c.status,
        c.sku,
        c.charge_date,
        case 
            when c.sku like "%learn%" then 'learn'
            else 'play'
        end as product
    from
        {{ ref('caf_charges') }} c
    where
        status in ('free_trial', 'paid')
),


ft_and_ft_conv_charges as (
    select
        c1.charge_id as ft_start_charge_id,
        c2.charge_id as ft_conv_charge_id
    from
        # c1 is the trial_start annd c2 is the conversion
        c as c1
        left join c as c2 on 
            c1.user_id = c2.user_id 
            and c2.charge_date between date_add(c1.charge_date, INTERVAL 5 day) and date_add(c1.charge_date, INTERVAL 25 day)
            and c1.sku = c2.sku
    where
        c1.status = 'free_trial'
),


ft_per_product_user as (
    select
        user_id,
        product,
        sku,
        payment_source,
        charge_id,
        charge_date,
        status,
        row_number() over (partition by user_id, product, status order by charge_date asc) as charge_order
    from
        c
    where
        (c.charge_id in (select distinct ft_start_charge_id from ft_and_ft_conv_charges)
            or c.charge_id in (select distinct ft_conv_charge_id from ft_and_ft_conv_charges))
),


pre_final as (
    select
        *,
        case when status = 'free_trial' and product = 'learn' then charge_date end as learn_trial_start_date,
        case when status = 'paid' and product = 'learn' then charge_date end as learn_trial_converted_date,
        case when product = 'learn' then payment_source end as learn_ft_payment_source,
        case when product = 'learn' then sku end as learn_trial_sku,
        case when status = 'free_trial' and product = 'play' then charge_date end as play_trial_start_date,
        case when status = 'paid' and product = 'play' then charge_date end as play_trial_converted_date,
        case when product = 'play' then payment_source end as play_ft_payment_source,
        case when product = 'play' then sku end as play_trial_sku
    from
        u left join ft_per_product_user pu
        using (user_id)
    where
        charge_order = 1 or charge_order is null
),

final as (
    select 
        user_id,
        max(learn_trial_start_date) as learn_trial_start_date,
        max(learn_trial_converted_date) as learn_trial_converted_date,
        max(learn_ft_payment_source) as learn_ft_payment_source,
        max(play_trial_start_date) as play_trial_start_date,
        max(play_trial_converted_date) as play_trial_converted_date,
        max(play_ft_payment_source) as play_ft_payment_source,
        max(play_trial_sku) as play_trial_sku,
        max(learn_trial_sku) as learn_trial_sku
    from 
        pre_final 
    group by 
        user_id
)

select * from final








