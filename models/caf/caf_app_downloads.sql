select 
    id as download_id,
    source,
    build,
    country,
    downloads,
    case
        when source = 'ios' then 'iOS'
        when source = 'android' then 'Android'
        else 'NOT CATEGORIZED' 
    end as os,
    date(date) as download_date
from `celtic-music-240111.aws_kinedu_app_import.app_downloads`