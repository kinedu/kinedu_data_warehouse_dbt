select 
    id as refund_id,
    sale_id,
    charge_id,
    DATETIME(created_at) as refund_datetime,
    refund_date
from 
    `celtic-music-240111.aws_kinedu_app_import.refunds`