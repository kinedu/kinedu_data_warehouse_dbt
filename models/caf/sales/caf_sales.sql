with final as (
  select
    s.id as sale_id,
    s.user_id as user_id,
    s.usd_amount as usd_amount,
    date(s.created_at) as sale_date,
    s.month_key as month_key,
    cast(s.renewed_automatically as BOOL) as renewed_automatically,
    case
      when s.sku like "%lifetime%" then 'lifetime'
      when s.sku like "%_12%" or s.sku like "%partners%" then 'yearly'
      when s.sku like "%_1%" then 'monthly'
      when s.sku like "%_6%" then 'semesterly'
      when s.sku like "%_3%" then 'quarterly'
      else 'other'
    end as plan_type,
    s.sku as sku,
    s.payment_processor as payment_processor,
    case 
      when s.sku like "%partners%" then true
      when s.payment_processor like "%partner%" then true
      else false
    end as is_partner_sku
  from
    `celtic-music-240111.aws_kinedu_app_import.sales` s
  where
    #s._fivetran_deleted is false and 
    s.fraud =0
    and s.livemode =1
    and s.payment_status = 'paid'
    -- and s.payment_status in ('paid', 'refund', 'canceled')
)

select * from final where user_id in ( 
    select 
      distinct user_id 
    from 
      {{ ref('caf_users')}}
  )