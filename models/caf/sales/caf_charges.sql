with final as (
  select
    c.id,
    -- Have to do this case because there was a mistake on the source in the remote configs during these dates.
    CASE
        WHEN c.sku = "kinedu_learn_3_lt_ft" AND (date(c.created_at) BETWEEN date("2022-01-27") AND date("2022-03-31")) THEN "paywallPlayLearn"
        ELSE c.payment_source
    END AS payment_source,
    c.sale_id as sale_id,
    c.status,
    c.sku,
    c.usd_amount_paid,
    c.user_id,
    c.test_type,
    date(c.created_at) as charge_date,
    oc.name as code_name, 
    oc.code, 
    c.params
    from
    `celtic-music-240111.aws_kinedu_app_import.charges` c
  -- These 2 tables are to obtain info about the apple coupons, which are then used as a filter in Tableau
  LEFT JOIN `celtic-music-240111.aws_kinedu_app_import.offer_codes_redeems` ocr on c.id = ocr.charge_id
  LEFT JOIN `celtic-music-240111.aws_kinedu_app_import.offer_codes` oc ON oc.id = ocr.offer_code_id
  where
    c.fraud =0
    and c.testmode =0
    and (c.status = 'paid' or c.status = 'free_trial')
    -- and (c.status = 'paid' or c.status = 'free_trial' or c.status = 'refund' or c.status = 'canceled')
)

select * from final where user_id in ( 
    select 
      distinct user_id 
    from 
      {{ ref('caf_users')}}
  )