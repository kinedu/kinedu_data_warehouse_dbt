select 
    id as mkt_promo_code_id, 
    name,
    code,
    created_at as code_created_at,
    end_datetime,
    cast(active as BOOL) as active,
    start_datetime,
    purchases
from `celtic-music-240111.aws_kinedu_app_import.mkt_promo_codes`
#where _fivetran_deleted is false 