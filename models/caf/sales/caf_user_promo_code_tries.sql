select 
    ROW_NUMBER() over (partition by user_id, mkt_promo_code_id order by created_at desc) as flag,
    mkt_promo_code_id,
    user_id,
    created_at,
    updated_at
from 
    `celtic-music-240111.aws_kinedu_app_import.user_mkt_promo_code_tries`
where 
__hevo__marked_deleted is false
