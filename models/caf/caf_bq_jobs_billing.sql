/*
DECLARE gb_divisor INT64 DEFAULT 1024*1024*1024;
DECLARE tb_divisor INT64 DEFAULT gb_divisor*1024;
DECLARE cost_per_tb_in_dollar INT64 DEFAULT 5;
DECLARE cost_factor FLOAT64 DEFAULT cost_per_tb_in_dollar / tb_divisor;
SELECT
  ROUND(SUM(total_bytes_processed) / gb_divisor,2) as bytes_processed_in_gb,
  ROUND(SUM(IF(cache_hit != true, total_bytes_processed, 0)) * cost_factor,4) as cost_in_dollar,
  user_email,  
  total_bytes_billed,
  creation_time,
  job_id,
  query,
  total_bytes_processed
FROM (
  (SELECT * FROM `region-us`.INFORMATION_SCHEMA.JOBS_BY_PROJECT)
)
WHERE
  creation_time >= '2020-10-01'
GROUP BY 
  user_email,  
  total_bytes_billed,
  creation_time,
  job_id,
  query,
  total_bytes_processed
order by total_bytes_billed desc;

5/1024*1024*1024*1024
*/
SELECT
  # ROUND(SUM(total_bytes_processed) / gb_divisor,2) as bytes_processed_in_gb,
  ROUND(SUM(total_bytes_processed) / (1073741824),2) as bytes_processed_in_gb,
  # ROUND(SUM(IF(cache_hit != true, total_bytes_processed, 0)) * cost_factor,4) as cost_in_dollar,
  ROUND(SUM(IF(cache_hit != true, total_bytes_processed, 0)) * (4.547473508864641E-12),4) as cost_in_dollar,
  user_email,  
  total_bytes_billed,
  creation_time,
  job_id,
  query,
  total_bytes_processed
FROM (
  (SELECT * FROM `region-us`.INFORMATION_SCHEMA.JOBS_BY_PROJECT)
)
WHERE
  creation_time >= '2020-10-01'
GROUP BY 
  user_email,  
  total_bytes_billed,
  creation_time,
  job_id,
  query,
  total_bytes_processed
# order by total_bytes_billed desc