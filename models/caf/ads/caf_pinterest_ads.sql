select
  "Kinedu" as business_unit,
  "Pinterest" as network,
  ch.name as campaign_name,
  case
    when ch.name like "%iOS%" 
    or ch.name like "%IOS%" 
    or ch.name like "%iOs%" then 'iOS'
    when 
      ch.name like "%Android%" 
      or ch.name like "%ANDROID%"
      or ch.name like '%aOS%'
      or ch.name like "%android%" then 'Android'
    else 'Unknown'
  end as os,
  date(cr.date) as date,
  cr.clickthrough_1 as clicks,
  cr.impression_1 as impressions,
  cr.spend_in_micro_dollar/1000000 as spend,
  case 
    when REGEXP_EXTRACT(ch.name, r" [A-Z]{2} ")  is not null then 
    replace(REGEXP_EXTRACT(ch.name, r" [A-Z]{2} "),' ', '')
     when REGEXP_EXTRACT(ch.name, r" [A-Z]{2}$")  is not null then replace(REGEXP_EXTRACT(ch.name, r" [A-Z]{2}$"),' ', '')
    when ch.name like '%Canada%' then 'CA'
    when ch.name like '%USA%' then 'US'
    when ch.name like '%Mexico%' then 'MX'
    when ch.name like '%[UK]%' then 'UK'
    else 'Unknown'
  end as country
from
  `celtic-music-240111.pinterest_ads.campaign_report` cr
  join (select name, id from `celtic-music-240111.pinterest_ads.campaign_history` group by name, id) ch on cast(ch.id as int64) = cast(cr.campaign_id as int64)
