/*TODO: add test asserting local_spend_currency is USD*/
select
  "Kinedu" as business_unit,
  "Apple Search Ads" as network,
  ch.name as campaign_name,
  case
    when ch.name like "%iOS%" 
    or ch.name like "%IOS%" 
    or ch.name like "%iOs%" then 'iOS'
    when 
      ch.name like "%Android%" 
      or ch.name like "%ANDROID%"
      or ch.name like '%aOS%'
      or ch.name like "%android%" then 'Android'
    else 'Unknown'
  end as os,
  impressions,
  local_spend_amount as spend,
  taps as clicks,
  date(date) as date,
  case 
    when REGEXP_EXTRACT(ch.name, r" [A-Z]{2} ")  is not null then 
    replace(REGEXP_EXTRACT(ch.name, r" [A-Z]{2} "),' ', '')
     when REGEXP_EXTRACT(ch.name, r" [A-Z]{2}$")  is not null then replace(REGEXP_EXTRACT(ch.name, r" [A-Z]{2}$"),' ', '')
    when ch.name like '%Canada%' then 'CA'
    when ch.name like '%USA%' then 'US'
    when ch.name like '%Mexico%' then 'MX'
    when ch.name like '%[UK]%' then 'UK'
    else 'Unknown'
  end as country
from
    `celtic-music-240111.apple_ads_import.campaign_report` cr
    join (select name, id from `celtic-music-240111.apple_ads_import.campaign` group by name, id) ch on ch.id = cr.campaign_id
