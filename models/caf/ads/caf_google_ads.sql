select
  "Kinedu" as business_unit,
  "Google" as network,
  g_ads.campaign as campaign_name,
  null as ad_name,
  case
    when g_ads.campaign like "%iOS%" 
    or g_ads.campaign like "%IOS%" 
    or g_ads.campaign like "%iOs%" then 'iOS'
    when 
      g_ads.campaign like "%Android%" 
      or g_ads.campaign like "%ANDROID%"
      or g_ads.campaign like '%aOS%'
      or g_ads.campaign like "%android%" then 'Android'
    else 'Unknown'
  end as os,
  g_ads.clicks,
  g_ads.impressions,
  ROUND((g_ads.cost/1000000)/19.5, 3) as spend, /*fix conversion error and pass to 19.5 mxn to usd
    Also, for some reason data needs to be divided by 1 million in order to get right data*/ 
  date(g_ads.start_date) as date, -- Time zone is in Monterrey
  case 
    when REGEXP_EXTRACT(g_ads.campaign, r" [A-Z]{2} ")  is not null then 
    replace(REGEXP_EXTRACT(g_ads.campaign, r" [A-Z]{2} "),' ', '')
    when REGEXP_EXTRACT(g_ads.campaign, r" [A-Z]{2}$")  is not null then replace(REGEXP_EXTRACT(g_ads.campaign, r" [A-Z]{2}$"),' ', '')
    when g_ads.campaign like '%Canada%' then 'CA'
    when g_ads.campaign like '%USA%' then 'US'
    when g_ads.campaign like '%Mexico%' then 'MX'
    when g_ads.campaign like '%[UK]%' then 'UK'
    else 'Unknown'
  end as country
from
    `celtic-music-240111.google_ads_import.campaign_performance_report` g_ads
WHERE (lower(campaign) NOT LIKE "%podia%" AND lower(campaign) NOT LIKE "%master%") /*to remove the data from master class campaigns as requested by MKT*/
ORDER BY date
