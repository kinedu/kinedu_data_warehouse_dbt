select
  "Kinedu" as business_unit,
  "Tiktok" as network,
  null as adset_id,
  null as adset_name,
  null as ad_name,
  case 
    when campaign_name like "%ES%" or campaign_name like "%ESP" or campaign_name like "%MX%" then 'es'
    when campaign_name like "%BR%" or campaign_name like "%PT%" then 'pt'
    when campaign_name like "%EN%" or campaign_name like "%US%" or campaign_name like "%UK%" or campaign_name like "%IN - AAA%" or campaign_name like "%AS/AF%" or campaign_name like "%AU - %" then 'en'
    else null
  end as kinedu_language,
  case
    when campaign_name like "%iOS%" 
    or campaign_name like "%IOS%" 
    or campaign_name like "%iOs%" then 'iOS'
    when 
      campaign_name like "%Android%" 
      or campaign_name like "%ANDROID%"
      or campaign_name like '%aOS%'
      or campaign_name like "%android%" then 'Android'
    when
      campaign_name like '%Web%' then 'Web'
    else 'Unknown'
  end as os,
  CAST(impressions AS FLOAT64) as impressions,
  CAST(spend AS FLOAT64) as spend,
  CAST(clicks AS FLOAT64) as clicks,
  date(stat_time_hour) as date,
  'Unknown' as country
from `celtic-music-240111.hevo_dataset_celtic_music_240111_pun9.tiktok_campaign_hourly_report_v2` tiktok
JOIN `celtic-music-240111.hevo_dataset_celtic_music_240111_pun9.tiktok_campaigns_v2` campanas ON campanas.campaign_id = tiktok.campaign_id