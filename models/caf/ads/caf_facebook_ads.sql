select
  case
    when account_id in (44768771,114741883165557,1644779228923789,198129391375942,7271985552884505) then "Kinedu"
    else "Other"
  end as business_unit,
  "Facebook" as network,
  campaign_name,
  adset_id,
  adset_name,
  ad_name,
  case 
    /*
    -- when adset_name like "%ES - AAA%" or adset_name like "%ES -%" or adset_name like "%ESP -%" or adset_name like "%ES Centroamerica%" then 'es'
    when adset_name like "%ES - AAA%" or adset_name like "%ES -%" or adset_name like "%ESP -%" or adset_name like "%ES Centroamerica%" or adset_name like "%MX%" then 'es'
    when adset_name like "%BR - AAA%" or adset_name like "%BR -%" or adset_name like "%PT -%" then 'pt'
    -- when adset_name like "%EN - AAA%" or adset_name like "%EN -%" or adset_name like "%US - AAA%" or adset_name like "%US - EN%" or adset_name like "%IN - AAA%" or adset_name like "%AS/AF%" then 'en'
    when adset_name like "%| EN |%" or adset_name like "%EN - AAA%" or adset_name like "%EN -%" or adset_name like "%US - AAA%" or adset_name like "%US - EN%" or adset_name like "%IN - AAA%" or adset_name like "%AS/AF%" or adset_name like "%UK%" or adset_name like "%US%" then 'en'
    */
    when adset_name like "%ES%" or adset_name like "%ESP" or adset_name like "%MX%" then 'es'
    when adset_name like "%BR%" or adset_name like "%PT%" then 'pt'
    when adset_name like "%EN%" or adset_name like "%US%" or adset_name like "%UK%" or adset_name like "%IN - AAA%" or adset_name like "%AS/AF%" or adset_name like "%AU - %" then 'en'
    else null
  end as kinedu_language,
  case
    when campaign_name like "%iOS%" 
    or campaign_name like "%IOS%" 
    or campaign_name like "%iOs%" then 'iOS'
    when 
      campaign_name like "%Android%" 
      or campaign_name like "%ANDROID%"
      or campaign_name like '%aOS%'
      or campaign_name like "%android%" then 'Android'
    when
      campaign_name like '%Web%' then 'Web'
    else 'Unknown'
  end as os,
  impressions,
  spend,
  inline_link_clicks as clicks,
  date(date_start) as date,
  case
    when country is null or country = 'unknown' then 'Unknown'
    else country
  end as country 
from
  `celtic-music-240111.FB_ads_import.ad_insights_country` fa
where account_id in (44768771,114741883165557,1644779228923789,198129391375942,433794644542668,7271985552884505) 
#requested by Luis and MKT Team
AND (lower(campaign_name) NOT LIKE "%podia%" AND lower(campaign_name) NOT LIKE "%master%" AND lower(campaign_name) NOT LIKE "%shop%") /*to remove the data from master class campaigns and shopify as requested by MKT*/