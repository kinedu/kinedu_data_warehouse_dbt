{{ config( materialized = "table") }}

with s as (
  select
    user_id,
    sale_date,
    id,
    sale_id,
    payment_processor,
    payment_source,
    sku,
    usd_amount,
    renewed_automatically,
    month_key,
    plan_type,
    is_partner_sku,
    code_name, 
    code, 
    case
      when plan_type = 'yearly' then 12
      when plan_type = 'monthly' then 1
      when plan_type = 'quarterly' then 3
      else 1
    end as service_months
  from
    {{ ref('ba_sales') }}
  where
    sku like "%learn%"
    and sku not like "%partners_license%"
    and payment_processor not like "%partners_license%"
),

n as (
  select
    *
  from
    {{ ref('n_1_to_10000') }}
),


ordered as (
  select
    *,
    s.usd_amount/s.service_months as distributed_amount,
    date_add(s.sale_date, interval n.num - 1 MONTH) as service_date
  from
    s
    join n on s.service_months >= n.num
  where
    s.user_id is not null
  order by
    user_id,
    sale_date,
    id,
    num
)

select *,ROW_NUMBER() OVER(order by user_id, sale_date, id, num) AS inc_id from ordered