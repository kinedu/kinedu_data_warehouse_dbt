{{ config( materialized = "table") }}

with n as (
  select
    *
  from
    {{ ref('n_1_to_12') }}
),

ordered as (
  select
    *,
    s.usd_amount/s.service_months as distributed_amount,
    date_add(s.sale_date, interval n.num - 1 MONTH) as service_date
  from
    {{ref('pre_inter_track_subscriptions_ba_sales')}} s
    join n on s.service_months >= n.num
  where
    s.user_id is not null
  order by
    user_id,
    sale_date,
    id,
    num
)

select *, ROW_NUMBER() OVER(order by user_id, sale_date, id, num) AS inc_id from ordered