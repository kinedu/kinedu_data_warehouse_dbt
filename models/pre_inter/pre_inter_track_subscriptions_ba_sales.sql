{{ config( materialized = "table") }}

  select
    user_id,
    sale_date,
    id,
    sale_id,
    payment_processor,
    payment_source,
    sku,
    usd_amount,
    renewed_automatically,
    month_key,
    plan_type,
    is_partner_sku,
    code_name, 
    code, 
    case
      when plan_type = 'yearly' then 12
      when plan_type = 'monthly' then 1
      when plan_type = 'semesterly' then 6
      when plan_type = 'lifetime' then 1
      when plan_type = 'quarterly' then 3
      else 1
    end as service_months
  from
    {{ ref('ba_sales') }}
  where
    sku not like "%learn%"
    and sku not like "%partners_license%"
    and payment_processor not like "%partners_license%"