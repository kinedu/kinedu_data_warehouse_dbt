with final as (
  select
    distinct country
  from
    {{ ref('caf_users')}}
)

select * from final
