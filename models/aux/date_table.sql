SELECT
  date
FROM UNNEST(GENERATE_DATE_ARRAY('2015-01-01', '2030-12-31')) AS date
ORDER BY date
