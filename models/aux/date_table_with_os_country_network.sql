with dates as (
  select
    *
  from
    {{ ref('date_table')}}
),

os as (
  select
    *
  from
    {{ ref('all_os')}}
),

networks as (
  select
    *
  from
    {{ ref('all_networks')}}
),

countries as (
  select
    *
  from
    {{ ref('all_countries')}}
),

final as (
  select
    *
  from
    dates,
    os,
    networks,
    countries
)

select * from final
