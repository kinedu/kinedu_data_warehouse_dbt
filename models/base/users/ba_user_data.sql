{{ config( materialized = "table") }}

with u as (
  select
    *
  from
    {{ ref('caf_users') }}
),

uxd as (
  select
    *
  from
    {{ ref('caf_user_extra_data') }}
),

m as (
  select
    user_id,
    account_id,
  from
    {{ ref('caf_memberships') }}
),

a as (
  select
    *
  from
    {{ ref('caf_accounts') }}
),

ft as (
  select
    *
  from
    {{ ref('caf_free_trials') }}
),

kr as (
  select
    kinedu_region,
    country
  from
    `celtic-music-240111.dbt_prod_aux.countries_kinedu_regions`
),

# mp_plan ahora se compone de premium (premium y play), learn y freemium
final as (
  select
    *,
    case 
      when plan_key like "%premium%" or plan_key like "%play%" then "premium"
      when plan_key like "%learn%" then "learn"
      else "freemium"
    end as mp_plan,
  from
    u
    join uxd using (user_id)
    left join m using (user_id)
    left join ft using (user_id)
    left join a using (account_id)
    left join kr using (country)
)

select * from final
