with s as (
  select
    sale_id, user_id, sale_date, usd_amount, month_key, renewed_automatically, plan_type, payment_processor, is_partner_sku
  from
    {{ ref('caf_sales')}}
),

c as (
  select
    * except (user_id)
  from
    {{ ref('caf_charges')}}
  where
    status = 'paid'
    -- status in ('paid', 'refund', 'canceled')
),

final as (
  select
    *
  from
    s
    join c using (sale_id)
)

select * from final
