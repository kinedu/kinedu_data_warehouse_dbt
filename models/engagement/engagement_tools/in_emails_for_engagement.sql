{{ config( materialized = "view") }}

with pre as (
	SELECT
		*
	FROM
		{{ ref('in_emails_for_engagement_preprocessing') }} as users
),

current_plan as (
	SELECT 
		u.user_id,
		u.mp_plan
	FROM 
		{{ ref('caf_users_direct_view') }} u 
) 

SELECT
	pre.*,
	current_plan.mp_plan
FROM
	pre
	join current_plan on pre.user_id = current_plan.user_id

##some users may be lost on the join because of not having a membership/account assigned 