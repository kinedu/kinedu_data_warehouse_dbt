{{ config( materialized = "view") }}


with u as (
	SELECT
		user_id,
		email,
		kinedu_language as locale,
		user_name,
		created_at,
		country,
		timezone,
		mp_plan
	FROM
		{{ ref('ba_user_data') }}
	WHERE
		email not in (SELECT distinct email FROM {{ ref('caf_blacklisted_emails') }})
),


ep as (
	SELECT
		user_id,
		subscribed
	FROM
		{{ ref('caf_email_preferences') }}
),


te_os as (
  SELECT
    user_id,
    os
  FROM 
  	{{ ref('caf_users_latest_os') }}
),


cu as (
	SELECT
		u.user_id,
		u.email,
		u.locale,
		u.user_name,
		u.created_at,
		u.timezone,
		u.country,
		u.mp_plan
	FROM
		u 
		left join ep on u.user_id = ep.user_id
	WHERE
		(ep.user_id is null or ep.subscribed = true)
),


final as (
	SELECT
		distinct cu.user_id,
		cu.email,
		cu.locale,
		cu.user_name,
		cu.created_at,
		cu.timezone,
		cu.country,
		cu.mp_plan,
		te_os.os
	FROM
		cu
		left join te_os using (user_id)
)


select 
	user_id, 
	email, 
	locale, 
	os, 
	user_name, 
	created_at,
	country as mp_country,
	mp_plan,
	timezone as mp_timezone 
from
	final










