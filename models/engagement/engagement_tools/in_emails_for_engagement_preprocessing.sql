{{ config( materialized = "table") }}

with u as (
	SELECT
		u.user_id,
		u.email,
		u.kinedu_language as locale,
		u.user_name,
		u.created_at,
		u.country,
		u.timezone,
		case when u.email LIKE "%gmail%" or u.email LIKE "%hotmail%" then true else false end as not_high_care
	FROM
		{{ ref('ba_user_data') }} u
    	left outer join (SELECT user_id FROM {{ ref('caf_memberships') }} WHERE JSON_EXTRACT_SCALAR(new_user_data, "$['organization_id']") = '1298' and status = 'classrooms_accepted') m on u.user_id = m.user_id
    	left outer join (SELECT distinct email FROM {{ ref('caf_blacklisted_emails') }}) b on b.email = u.email
	WHERE
		b.email is null
    	and u.email not like 'facebook-%@farias.mx' 
    	and m.user_id is null
),

ep as (
	SELECT
		user_id,
		subscribed
	FROM
		{{ ref('caf_email_preferences') }}
),

te_os as (
  SELECT
    user_id,
    os
  FROM 
    {{ ref('caf_users_latest_os') }}
),


-- en as (
-- 	SELECT
-- 		distinct destination_email
-- 	FROM
-- 		{{ ref('caf_email_notifications')}}
-- 	WHERE
-- 		timestamp_diff(current_timestamp(), open_timestamp, day) <= 90
-- ),


cu as (
	SELECT
		u.user_id,
		u.email,
		u.locale,
		u.user_name,
		u.created_at,
		u.timezone,
		u.country,
		u.not_high_care as apply_for_email
	FROM
		u 
		## left join m on u.email = m.email
		-- left join en on u.email = en.destination_email
		left join ep on u.user_id = ep.user_id
	WHERE
		(ep.user_id is null or ep.subscribed = true)
),


final as (
	SELECT
		distinct cu.user_id,
		cu.email,
		cu.locale,
		cu.user_name,
		cu.created_at,
		cu.timezone,
		cu.country,
		te_os.os
	FROM
		cu
		left join te_os using (user_id)
	WHERE
		apply_for_email
		or email in ('edbentinck@gmail.com', 'a.elosua@me.com')
)


select 
	user_id, 
	email, 
	locale, 
	os, 
	user_name, 
	created_at,
	country as mp_country,
	timezone as mp_timezone 
from
	final

