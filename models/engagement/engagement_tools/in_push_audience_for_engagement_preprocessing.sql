{{ config( materialized = "table") }}
with ud as (
	SELECT
		user_id,
		account_id,
        email,
        user_name,
        kinedu_language as locale,
        created_at,
        country,
        timezone
	FROM
		{{ ref('ba_user_data') }} as users /*`celtic-music-240111.dbt_prod_base.ba_user_data`*/
),

ekd as (
	SELECT 
		client_id, 
		identifier 
	FROM 
		{{ ref('caf_engagement_kinedu_devices') }}
),

ekdt as (
	SELECT 
	 	device_identifier 
	FROM 
		{{ ref('caf_engagement_kinedu_device_tokens') }} as tokens
    WHERE 
		tokens.active IS TRUE AND tokens.endpoint_arn IS NOT NULL
),

bb as (
    SELECT 
		baby_name, 
		account_id 
	FROM 
		{{ ref('caf_babies_data') }}
),

te_os as (
	SELECT
    	user_id,
    	os
  	FROM 
    	{{ ref('caf_users_latest_os') }} /*celtic-music-240111:dbt_prod_caf.caf_users_latest_os*/
),

pre_final as (
    SELECT 
		ud.user_id,
        ud.email,
        ud.user_name,
		bb.baby_name,
        ud.locale,
        ud.created_at,
        ud.country,
        ud.timezone
    FROM 
		ud 
	    INNER JOIN ekd on ud.user_id = ekd.client_id
	    INNER JOIN ekdt on ekdt.device_identifier = ekd.identifier
	    LEFT JOIN bb on bb.account_id = ud.account_id
),

final as (
	SELECT
		pre_final.user_id,
		pre_final.email,
		pre_final.user_name,
		pre_final.baby_name,
        pre_final.locale,
        pre_final.created_at,
        pre_final.country,
        pre_final.timezone,
		te_os.os
	FROM 
		pre_final
		LEFT JOIN te_os using (user_id)
)

SELECT
	user_id,
	email,
	user_name,
	baby_name,
    locale,
    created_at,
    country,
    timezone,
	os
FROM 
	final
ORDER BY 
	user_id, 
	email