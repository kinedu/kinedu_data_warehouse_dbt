{{ config( materialized = "view") }}


SELECT
	destination_email,
	notification_type,
	open_timestamp
FROM
	`celtic-music-240111.dynamodb_engagement_import.engagement_kinedu_email_notifications`