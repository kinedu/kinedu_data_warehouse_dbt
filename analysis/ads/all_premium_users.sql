select
  u.id as user_id
from
  `celtic-music-240111.aws_kinedu_app.users` u
  join `celtic-music-240111.aws_kinedu_app.user_extra_data` uxd on u.id = uxd.user_id
  left join `celtic-music-240111.aws_kinedu_app.accounts` a on u.id = a.owner_id
where
  u._fivetran_deleted is false
  and uxd._fivetran_deleted is false
  and a._fivetran_deleted is false
  and u.email not like "%@test.com%"
