## _dbt_ documentation for Kinedu's Analytics & Data Science infrastructure

### Installation

First step is to download *dbt* from [dbt's official website](https://docs.getdbt.com/docs/macos). We recommend installing it using *pip* as we are also going to be using *python* for other analyses. *Python* and *pip* can be downloaded from [Anaconda's official website](https://www.anaconda.com/distribution/). Please install the **python 3.X version**.

After downloading and installing dbt you need to clone this repository 
`git clone https:// repository URL at the top of bitbucket's page`

Then you must to run the following command on a linux terminal inside the repository's main folder. (something like... `cd kinedu_data_warehouse`)

`dbt deps`

This will install the package dependencies.

#### dbt_project.yml
Afterwards installation, another important part of the project is to set your own  **dbt_project.yml**. 
Which is used for the following information:
- This file contains the project structure and tells dbt where to find each part of the project inside the folders.
- It also tells dbt what *profile* to use.
- Models materialization, schema and other important configurations while getting compiled.

In order to do that, you must run the following command in "/kinedu_data_warehouse_dbt (master)". To maintain the collaborative structure of this repository the attribute `profile:` has to set to the value `'dbt_hector'`: 

`dbt init dbt_name`

#### profiles.yml
Subsequently, you'll get a success message on your console and it will tell you that the next step is to set up your profiles.yml file which you will find by running: 

`open /Users/your_user_name/.dbt` 

** Do not close this finder window, it will be needed a few steps ahead.

More information about how to set it can be found [here](https://docs.getdbt.com/docs/configure-your-profile). 

Basically we set up the credentials that dbt is going to use to write into our data warehouse, in this case BigQuery. We will set up 2 target with different output datasets. One for development and another on for production. It is important to leave the default as *dev* since we don't want to be messing with the production views and tables accidentally.

To maintain the collaborative structure of this repository *at the moment each member of the Analytics & DS team has his own dataset for `dev` environment *

**Example**:

```
dbt_dev:
  target: dev
  outputs:
    dev:
      type: bigquery
      method: oauth
      project: celtic-music-240111
      dataset: dbt_hector
      threads: 1
      timeout_seconds: 300
    prod:
      type: bigquery
      method: service-account
      project: celtic-music-240111
      dataset: dbt_prod
      threads: 1
      keyfile: /Users/path-to-key-file/keyfile_dbt.json
      timeout_seconds: 300
``` 

What you will do is, use a text editor to create a file that looks exactly as the one above, just fill it with your information, save it with the name "profiles.yml" and replace the one you opened before for this new "profiles.yml". 

** About the keyfile: To add the path of this .json file you must ask a colleague to send it to you, change its name and save it in a folder outside the repository. 

Once you've finished the steps above there is one last thing to do...
### Big Query authentication 
In the following link, you will find [dbt's instructions to connect to BigQuery](https://docs.getdbt.com/docs/supported-databases/profile-bigquery/)    
Follow the oauth method, install gcloud and then activate the application-default account with: 

`gcloud auth application-default login`

### Project Structure

The project is divided in 4 main folders
- Models
- Macros
- Test
- Analysis

The **Models** folder contains the .SQL files that will be compiled into pure SQL that will run in BigQuery.

The **Macros** folder contains the Macros that are snippets of SQL that can be invoked like functions from models. Macros make it possible to re-use SQL between models in keeping with the engineering principle of DRY (Don't Repeat Yourself). Additionally, shared packages can expose macros that we can use in our own dbt project.

The **Test** folder has the test that we are going to build along with each dbt Model so we can run them and make sure we don't do any breaking changes to other models.

The **Analysis** folder is where we store custom analyses that don't quite fit into the *dbt model* structure. These *.sql* files will be version controlled along with all the project. These files will be compiled, but **not** executed into the data warehouse. The compiled sql files can be found in `target/compiled/kinedu_data_warehouse_dbt/analysis/`. The reason why these files are going to be compiled into another *.sql* file is because we are going to be able to use *dbt* functionality like `{{ ref(...) }}`. If you only want to *compile* the *.sql* files inside the *Analysis* folder you can just run `dbt compile`.

### Models' Structure

The Models directory is divided in 5 main folders
- caf
- base
- inter
- analytics
- aux

Each of these has an specific function.

The name **caf** has the meaning of 'clean and filter' so this folder contains subset of rows and columns from the raw or originals tables in the databases. For example from the table `users` from kinedu DB we are interested only in users (rows) who are not `test` and with a email not like `kinedu`and also we are only interested in analysing the columns `id`, `email`, `mp_country`, etc. not in columns like `idfa`. By only querying raw data in this directory, we ensure that changes in raw data's structure will only have to be changed in one place.

The **base** directory contains the models that query **caf** data from all our datasets. In this level starts the joins for create tables for direct analysis or for other tables more complex in the **inter** layer

The next step in our analytics workflow is the **inter** folder. It contains the models that are going to deal with the transformation of the data **before** it can be accessed by a data visualization tool like *Tableau*.

The **analytics** folder is the one that is going to have the views/tables that are used for custom analyses, automated analyses and data visualization (e.g. *Tableau*).

The **aux** directory is only for data auxiliary in the joints or ephemeral operations.

Inside each folder we have other subfolders to keep the models organized (e.g. ads, users)

#### Custom Schemas

In order to differentiate where each model is coming from, we put the prefix "caf_" for **caf** models, "ba_" for **base** models, "in_" for **inter** models and "an_" for **analytics** models.

We also added a *custom schema* to the models so thee materialization of the views/tables are done inside **different** dataset.

````
models:
  kinedu_data_warehouse_dbt:
    caf:
      materialized: table
      schema: caf
    base:
      materialized: table
      schema: base
    inter:
      materialized: table
      schema: inter
    analytics:
      materialized: table
      schema: analytics
    aux:
      materialized: table
      schema: aux
      enabled: true
````
This means that the output target (e.g. dbt_prod) will be divided in several datasets (e.g. dbt_prod_analytics)

### How to run dbt

The following commands should be executed on a terminal inside the project folder.


Use case | Command
------------ | -------------
Run all the models | `dbt run`
Run only certain models | `dbt run --models analytics`
Run dbt with prod as the target output | `dbt run --target prod`
Run dbt with prod as the target output and only certain models | `dbt run --target prod --models analytics`
Run tests | `dbt test`
Run all models with some incremental | `dbt run --full-refresh`
Run all models with some incremental in prod | `dbt run --target prod --full-refresh`




### Guidelines

- **Always** use **underscore_naming_convention**. Specially for *datasets*, *tables*, *views* and *columns* names. Note that every word is in lowercase.
- Table names are **plural**. (Ex. Employees)
- Columns are always **singular**. (Ex. name)
- Primary keys are named **id**.
- Foreign keys are named by the name of the table it references and the term *id*. (Ex. employee_id)
- Ideally, **all** analyses should be done on cleaned tables or views. Including custom analyses.

### A picture is worth a thousand words

A quick look at the graphic documentation and status of this [project](https://drive.google.com/file/d/1F5KrOwSiMPfDvOhViRFJrWRtD3vLOPlH/view?ts=5dba4ec7) (green-> done, yellow-> in progress and white-> planned)



### Refunds development task list
- Contestar estas preguntas
  + Cuantos usuarios tienen cancelaciones antes de comprar y cuanto tiempo pasa en promedio, std dev, min y max
  + Cuantos usuatios tienen cancelaciones despues de su ultima compra y cuanto tiempo pasa en promedio, std dev, min y max
  + Cuantos usuarios tienen cancelaciones entre compras y cual es el promedio de tiempo entre compra buena y compra mala, para los dos lados (antes y despues)
  + En que casos el usd_amount se vuelve 0 en sales y charges y cuales no
  + Hay algun campo que se quede sin modificar?
  + Que tan comun es que haya mas de una cancelacion en un mismo mes (promedio, std dev, min, max)
- Revisar cuanto cambia al agregar refunds y cancelled
- Revisar como se ven los casos en que un usuario hace mas de una compra en un mes
- Revisar que valor de usd_amount usar ya que en algunos casos cambio a 0 cuando se hace el refund
- Preguntas para la junta de pagos
  + usd_amount cambia para charges? cambia para sales? en que casos si, y en que no?
  + como manejar las cancelaciones despues de la ultima compra?








